import json
import matplotlib.pyplot as plt
import os.path


def printVACs():
	vacFilePath = "C:\\TestInstruments\\AIK\\Scripts\\VACs.txt"
	if os.path.isfile(vacFilePath):

		with open(vacFilePath, mode="r", encoding="UTF-8") as source_file:
			JSONString = source_file.read()
			if JSONString.find('label') != -1:
				VACData = json.loads(JSONString)
				needed2plotVACs = [VAC for VAC in VACData if VAC['needPlot'] == "yes"]
				VACsCount = len(needed2plotVACs)
				fig, axs = plt.subplots(VACsCount)

				for i in range(VACsCount):
					oneVac = needed2plotVACs[i]
					points = oneVac["points"]
					listX = [p[0] for p in points]
					listY = [p[1] for p in points]
					xLabel = oneVac['labelX'] + ", " + oneVac['unitX']
					yLabel = oneVac['labelY'] + ", " + oneVac['unitY']
					if VACsCount > 1:
						axs[i].plot(listX, listY)
						axs[i].set_xlabel(xLabel)
						axs[i].set_ylabel(yLabel)
					else:
						axs.plot(listX, listY)
						axs.set_xlabel(xLabel)
						axs.set_ylabel(yLabel)
				plt.show()
