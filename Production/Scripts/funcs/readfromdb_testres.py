def readfromdb_testres(connection, initdonn, duts_id, test_plan_id):
    import json
    import tkinter as tk
    from funcs import connectdb
    from tkinter import messagebox as mb

    testresults = []
    test_id = []
    outparam_id = []
    TName = []
    Outparam = []

    try:
        with connection.cursor() as cur:
            cur.execute("SELECT id, name, type, duts_id, short_name, unit FROM atedb.attr_test_param")
            buffer_attr = cur.fetchall()

            attr = buffer_attr[:]
            dict_attr = {}
            for h in range(0, len(attr)):
                dict_attr.setdefault(attr[h].pop('id'), attr[h])
            del attr

            # cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) +
            #             "' AND arm_id = " + str(arm_id)) - с выбором arm_id
            cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
            buffer_tstplan = cur.fetchall()[0]
            print("buffer_tstplan = " + str(buffer_tstplan))

        json_buffer = json.loads(buffer_tstplan.get('structure'))

        for i in range(0, len(json_buffer)):
            test_id.append(json_buffer[i].get('TestId'))

        for j in range(0, len(test_id)):
            with connection.cursor() as cur:
                # cur.execute("SELECT test_function, outparam FROM atedb.test_elements WHERE id = " +
                #             str(test_id[j]) + " AND arm_id = " + str(arm_id)) - с выбором arm_id
                cur.execute("SELECT test_function, outparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                buffer_tstelem = cur.fetchall()[0]

            TName.append(buffer_tstelem.get('test_function'))
            outparam_id.append(buffer_tstelem.get('outparam'))

        for n in range(0, len(outparam_id)):
            with connection.cursor() as cur:
                # cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(outparam_id[n]) +
                #             " AND arm_id = " + str(arm_id)) - с выбором arm_id
                cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(outparam_id[n]))
                buffer_tstparam = cur.fetchall()[0]

            Outparam.append(json.loads(buffer_tstparam.get('parametr')))

        for k in range(0, len(outparam_id)):
            testresults.append([{'TName': TName[k], 'NumResParams': len(Outparam[k])}, []])

            for m in range(0, len(Outparam[k])):
                testresults[k][1].append({'Ind': Outparam[k][m].get('Ind'),
                                          'Name': Outparam[k][m].get('Name'),
                                          'ShortName': dict_attr.get(Outparam[k][m].get('AttrId')).get('short_name'),
                                          'Unit': dict_attr.get(Outparam[k][m].get('AttrId')).get('unit'),
                                          'NormaTable': Outparam[k][m].get('NormaTable')})
        status = 0

    except Exception as e:
        print(e)
        status = 1
        answer = True

        root = tk.Tk()
        root.withdraw()

        while answer is True and status == 1:
            answer = mb.askyesno(title="Внимание!", message="Ошибка подключения к базе данных.\n Выполнить "
                                                            "повторную попытку подключения?", icon="question")
            if answer:
                status, connection = connectdb(initdonn)

            if status == 0:
                with connection.cursor() as cur:
                    cur.execute("SELECT id, name, type, duts_id, short_name, unit FROM atedb.attr_test_param")
                    buffer_attr = cur.fetchall()

                    attr = buffer_attr[:]
                    dict_attr = {}
                    for h in range(0, len(attr)):
                        dict_attr.setdefault(attr[h].pop('id'), attr[h])
                    del attr

                    # cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) +
                    #             "' AND arm_id = " + str(arm_id)) - выбор с arm_id
                    cur.execute("SELECT structure FROM atedb.test_plans WHERE duts_id = " + str(duts_id) + " AND id = '" + str(test_plan_id) + "'")
                    buffer_tstplan = cur.fetchall()[0]

                json_buffer = json.loads(buffer_tstplan.get('structure'))

                for i in range(0, len(json_buffer)):
                    test_id.append(json_buffer[i].get('TestId'))

                for j in range(0, len(test_id)):
                    with connection.cursor() as cur:
                        # cur.execute("SELECT test_function, outparam FROM atedb.test_elements WHERE id = " +
                        #             str(test_id[j]) + " AND arm_id = " + str(arm_id)) - выбор с arm_id
                        cur.execute("SELECT test_function, outparam FROM atedb.test_elements WHERE id = " + str(test_id[j]))
                        buffer_tstelem = cur.fetchall()[0]

                    TName.append(buffer_tstelem.get('test_function'))
                    outparam_id.append(buffer_tstelem.get('outparam'))

                for n in range(0, len(outparam_id)):
                    with connection.cursor() as cur:
                        # cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(outparam_id[n]) +
                        #             " AND arm_id = " + str(arm_id))
                        cur.execute("SELECT parametr FROM atedb.test_parameters WHERE id = " + str(outparam_id[n]))
                        buffer_tstparam = cur.fetchall()[0]

                    Outparam.append(json.loads(buffer_tstparam.get('parametr')))

                for k in range(0, len(outparam_id)):
                    testresults.append([{'TName': TName[k], 'NumResParams': len(Outparam[k])}, []])

                    for m in range(0, len(Outparam[k])):
                        testresults[k][1].append({'Ind': Outparam[k][m].get('Ind'),
                                                  'Name': Outparam[k][m].get('Name'),
                                                  'ShortName': dict_attr.get(Outparam[k][m].get('AttrId')).get('short_name'),
                                                  'Unit': dict_attr.get(Outparam[k][m].get('AttrId')).get('unit'),
                                                  'NormaTable': Outparam[k][m].get('NormaTable')})

        root.destroy()

    return testresults, test_id, status
