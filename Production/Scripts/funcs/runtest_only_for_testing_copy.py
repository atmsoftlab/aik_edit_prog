def runtest(DLLPath, XMlPath, inputData, bufferSize):
    import ctypes
    from funcs import errorFunc

    #print("inputdata = " + str(inputData))

    with open("C:\\TestInstruments\\AIK\\Scripts\\results.txt", mode="w", encoding="windows-1251") as result_file:
        for i in range(1000):
            result_file.write("0.01 + "+ str(i / 100) +",В,0,1\n")
    return 0, 'deskerror'

    # error - номер ошибки в Labview
    # iserror - переменная сообщающая об ошибке в тесте (0 - ошибки нет либо ее сбросили; 1 - ошибку возникшую в результате
    # выполнения теста не удалось сбросить (результат выполнения теста - "БРАК"); 2 - ошибка возникла на этапе инициализации,
    # еще до выполнения теста, (результат - "БРАК")
    # deskerror - переменная + описание ошибки
