def runtest(DLLPath, XMlPath, inputData, bufferSize):
    import ctypes
    from funcs import errorFunc

    #print("inputdata = " + str(inputData))

    newlib = ctypes.CDLL(DLLPath)
    exec('newlib.XMLWorker.argtypes = [ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_char), ctypes.c_int]')
    exec('newlib.XMLWorker.restype = ctypes.c_int')
    # outstr = np.full(bufferSize, b' ')
    bufferin = (ctypes.c_double * len(inputData))(*inputData)
    #bufferout = (ctypes.c_char * len(outstr))(*outstr)
    Path = bytes(XMlPath, encoding='utf8')
    BPath = (ctypes.c_char * len(Path))(*Path)
    error = newlib.XMLWorker(bufferin, BPath, len(inputData))

    # print("error = " + str(error))

    if error:
        # print("Code Error: {}".format(error))
        deskerror = errorFunc(error)
    elif error == 0:
        deskerror = ''

    return error, deskerror

    # error - номер ошибки в Labview
    # iserror - переменная сообщающая об ошибке в тесте (0 - ошибки нет либо ее сбросили; 1 - ошибку возникшую в результате
    # выполнения теста не удалось сбросить (результат выполнения теста - "БРАК"); 2 - ошибка возникла на этапе инициализации,
    # еще до выполнения теста, (результат - "БРАК")
    # deskerror - переменная + описание ошибки
