package sample;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class SelectionProgramBuffer {

    private static List<Node> Nodes = new ArrayList();

    public static void add(Node adding){
        Nodes.add(adding);
    }

    public static void clearNodes(){
        Nodes.clear();
    }

    public static List<Node> getNodes() {
        return Nodes;
    }

    public static void addAll(List<Node> nodes) {
        Nodes.addAll(nodes);
    }
}

