package sample;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import sample.controllers.*;
import sample.enums.DialogEndStatus;
import sample.enums.ExecutePointTypes;
import sample.external.workWithXMLStructure;
import sample.simple.ExecutingBounds;
import sample.simple.PathHolder;
import sample.simple.SourceTimeMode;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import static sample.Methods.getIndexMapForSelectedTreeItem;
import static sample.Methods.twoStateDialog;

public class ProgramObjectClass {
    private static final int MAX_CHANGES_COUNT_BEFORE_TEMP_SAVING = 10; //максимальное количество хранимых в объекте изменений до сохранения во временный файл
    private String filePath;
    private byte changeCount = 0; //Счетчик изменений программы
    private TreeView<String> struct_tree;
    private String prgName = "";
    private Document ProgramDocument;
    private Map<String, Map<String, Object>> GUIObjects;
    private Node selectedNode;
    private Node bufferedElement;
    private Element streamBlock;
    private int blockCount = 0;
    public TreeItem<String> newSelectedTreeItem;
    private Logger Log;
    public boolean ProgramSaved = false;
    private MainProgramEditWindowController MainController;
    private File[] tempFiles = new File[MAX_CHANGES_COUNT_BEFORE_TEMP_SAVING];
    private int allowUndoCounter = 0;
    private ProgramObjectClass selfLink;
    private NodeList headers;
    private boolean streamMakeTerminate;

    public ProgramObjectClass(){
        selfLink = this;
    }

    public void setLogger(Logger log) {
        this.Log = log;
    }

    public Document getProgramDocument() {
        return ProgramDocument;
    }

    public void setMainController(MainProgramEditWindowController mainController) {
        this.MainController = mainController;
    }

    public void setProgramName(String name){
        this.prgName = name;
    }

    public String getFilePath(){
        return filePath;
    }

    public int getBlockCount() {
        if (blockCount==0)
            blockCount = this.ProgramDocument.getElementsByTagName("block").getLength();
        return blockCount;
    }
    public ArrayList<Node> getMeasNodes() {
        ArrayList<Node> nodes = new ArrayList<Node>();
        int measCount = 0;
        try {
            if (selectedNode.getNodeName().equals("block"))
                return nodes;
            Element currentBlock = (Element)getParentElementByName(selectedNode, "block");
            NodeList params = currentBlock.getChildNodes();
            boolean stop = false;
            for (int i=0;i<params.getLength();i++){
                Node tempNode = params.item(i);
                if (tempNode.getNodeType()==Node.ELEMENT_NODE && tempNode.getNodeName().equals("param")){
                    if (tempNode.equals(selectedNode) || stop) {
                        stop = true;
                        break;
                    }
                    NodeList actions = tempNode.getChildNodes();
                    for(int j = 0;j<actions.getLength();j++){
                        Node tempActionNode = actions.item(j);
                        if (tempActionNode.getNodeType()==Node.ELEMENT_NODE && (tempActionNode.getNodeName().equals("currMeas") || tempActionNode.getNodeName().equals("voltMeas"))){
                            nodes.add(tempActionNode);
                            measCount++;
                        }
                        if (tempActionNode.equals(selectedNode)){
                            stop = true;
                            break;
                        }
                    }
                }
            }
        }catch (Exception e){
            Log.info("ProgramObjectClass: getMeasCount Error: "+e.fillInStackTrace());
        }
        return nodes;
    }

    public int getMeasCount(){
        return getMeasNodes().toArray().length;
    }

    public void setGUIObjects(Map<String, Map<String, Object>> GUIObjects) {
        this.GUIObjects = GUIObjects;
        initGUIObjects();
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public TreeView<String> getStruct_tree() {
        return struct_tree;
    }

    public boolean SaveProgramFile(){
        //для сохранения временных файлов
        boolean status = false;
        try {
            if (filePath!=null) {
                try {
                    reWriteResultsIfHaveIt(ProgramDocument.getElementsByTagName("result"));
                }catch (Exception exception){}
                makeStream();
                new workWithXMLStructure().saveXMLDocument2FileByFilePath(this.ProgramDocument, filePath);
                status = true;
                ProgramSaved = true;
            }else{
                if (getPath2saveNewFile()) {
                    SaveProgramFile();
                }
            }
        } catch (TransformerException e) {
            Log.info("Program file saving error:");
            e.printStackTrace();
        }finally {
            return status;
        }
    }

    public boolean SaveAsProgramFile() {
        boolean status = false;
        try{
            if (getPath2saveNewFile())
                status = SaveProgramFile();
        } catch (Exception e) {
            Log.info("Program file saving as error:");
            e.printStackTrace();
        }
        return status;
    }

    boolean getPath2saveNewFile(){
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(//
                new FileChooser.ExtensionFilter("XML", "*.xml"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));

        File dir = chooser.showSaveDialog((Stage) this.struct_tree.getScene().getWindow());
        if (dir != null) {
            filePath = dir.getAbsolutePath();
            PathHolder.setProgramPath(filePath.substring(0, filePath.lastIndexOf('\\') + 1));
            return true;
        }
        return false;
    }

    public void delOneChange(){
        File prevTempFile = getLastTempFile();
        if (prevTempFile!=null && allowUndoCounter>0) {
            try {
                //Node prevNode = getPrevious(selectedNode);
                this.ProgramDocument = new workWithXMLStructure().createNewXMLDocument(prevTempFile.getAbsolutePath());
                selectedNode = this.ProgramDocument.getElementsByTagName("prg").item(0).getChildNodes().item(0); //пока так
                reloadProgramFromDoc();
                changeCount--;
                allowUndoCounter--;
                if (getBlockCount()>0){
                    ((Pane) GUIObjects.get("panels").get("workPane")).setVisible(true);
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }
    }
    private String getStringForTreeFromNode(Node node){
        String result = "";
        switch (node.getNodeName()){
            case "switch":
                result = node.getTextContent()+(node.getAttributes().getNamedItem("status").getNodeValue().equals("1")?"_ВКЛ":"_ВЫКЛ");
                break;
            case "param":
            case "block":
                result = node.getAttributes().getNamedItem("name").getNodeValue();
                break;
            case "d_block":
                result = node.getAttributes().getNamedItem("name").getNodeValue()+"("+ node.getAttributes().getNamedItem("pro_name").getNodeValue() +")";
                break;
            case "pause":
                result = "Пауза: "+node.getAttributes().getNamedItem("value").getNodeValue()+"мс";
                break;
            case "result":
                result = "Результат:";
                break;
            case "voltageSource":
                result = "Зад.напряж.("+node.getAttributes().getNamedItem("id").getNodeValue()+":"+node.getAttributes().getNamedItem("voltage").getNodeValue()+";"+node.getAttributes().getNamedItem("voltageRange").getNodeValue()+
                        ";"+node.getAttributes().getNamedItem("currentLimit").getNodeValue()+")";
                break;
            case "voltageSourceReset":
                result = "Сброс зад.напр.("+node.getAttributes().getNamedItem("id").getNodeValue()+")";
                break;
            case "currentSourceReset":
                result = "Сброс зад.тока("+node.getAttributes().getNamedItem("id").getNodeValue()+")";
                break;
            case "voltMeas":
                result = node.getAttributes().getNamedItem("id").getNodeValue()+"("+node.getAttributes().getNamedItem("voltageRange").getNodeValue()+")";
                break;
            case "currentSource":
                result = "Зад.тока("+node.getAttributes().getNamedItem("id").getNodeValue()+":"+node.getAttributes().getNamedItem("current").getNodeValue()+";"+node.getAttributes().getNamedItem("currentRange").getNodeValue()+
                        ";"+node.getAttributes().getNamedItem("voltageLimit").getNodeValue()+")";
                break;
            case "currMeas":
                result = node.getAttributes().getNamedItem("id").getNodeValue()+"("+node.getAttributes().getNamedItem("currentRange").getNodeValue()+")";
                break;
            case "fullReset":
                result = "Общий сброс";
                break;
            case "moving":
                result = node.getAttributes().getNamedItem("Type").getNodeValue()+" X="+node.getAttributes().getNamedItem("x").getNodeValue()+node.getAttributes().getNamedItem("unit").getNodeValue()+"; Y="+node.getAttributes().getNamedItem("y").getNodeValue()+node.getAttributes().getNamedItem("unit").getNodeValue();
                break;
            case "header":
                result = "Заголовок: "+node.getTextContent();
                break;
            case "Vac":
                result = "ВАХ";
                break;
            default:
                result = node.getTextContent();
        }
        return result;
    }

    private String getImageNameForNode(Node node) {
        String nodeName = node.getNodeName();
        switch (nodeName){
            case "switch":
                return "relayImg";
            case "block":
                return "blockImg";
            case "d_block":
                return "d_blockImg";
            case "param":
                return "paramImg";
            case "pause":
                return "pauseImg";
            case "voltageSource": case "voltageSourceReset":
                return "VSourceImg";
            case "currentSource": case "currentSourceReset":
                return "ISourceImg";
            case "voltMeas":
                return "VMeasTree";
            case "currMeas":
                return "IMeasTree";
            case "fullReset":
                return "fullResetImg";
            case "moving":
                return "movingImg";
            case "header":
                return "headerImg";
            case "result": case "exp": case "unit": case "minNorm": case "maxNorm":
            case "VacSource":case "VacMeasure":case "VacPlotGainX":case "Vac":
            case "VacPlotGainY":case "VacUnitY":case "VacUnitX":case "VacSourceRange":
            case "VacMeasureDelay":
                return nodeName+"Img";
            default:
                return "image1";
        }
    }
    private void TreeGenerationFromNodeList(NodeList nList, TreeItem<String> parentTreeItem) {
        try {
            boolean parentTreeItemExpandedProperty = parentTreeItem.isExpanded();
            parentTreeItem.getChildren().clear();
            parentTreeItem.setExpanded(parentTreeItemExpandedProperty);
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node node = nList.item(temp);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    if (node.getNodeName().equals("stream")){
                        streamBlock = (Element) node;
                        continue;
                    }
                    TreeItem<String> tempTreeItem = new TreeItem<String>(getStringForTreeFromNode(node));
                    //refreshGUIObjectsByNode(node, false);
                    ImageView img = new ImageView(checkIsNodeIsBoundAndChangeImageIfNeed(node,"images/"+getImageNameForNode(node)+".png"));
                    tempTreeItem.setGraphic(img);
                    parentTreeItem.getChildren().add(tempTreeItem);
                    tempTreeItem.setExpanded(ExpandMapper.getExpanded(getIndexMapForSelectedTreeItem(tempTreeItem)));
                    setExpandedChangeListener(tempTreeItem);
                    if (node.equals(selectedNode)){
                        Log.info("Выделеный ранее элемент найден");
                        MultipleSelectionModel msm = struct_tree.getSelectionModel();
                        newSelectedTreeItem = tempTreeItem;
                        int row = struct_tree.getRow(newSelectedTreeItem);
                        msm.select( row );
                        //refreshTreeBySelectedItem(TempTreeItem);
                    }
                    if (node.hasChildNodes()) {
                        TreeGenerationFromNodeList(node.getChildNodes(), tempTreeItem);
                    }
                }
            }
        }catch (Exception e){Log.info("TreeGenerationFromNodeList Error: "+e.getMessage());}

    }

    private Image checkIsNodeIsBoundAndChangeImageIfNeed(Node node, String imagePath) {
        try {
            if (ExecutingBounds.isStartNode(node)){
                return SwingFXUtils.toFXImage(Methods.putImageUnderExistImage("images/startBoundImg.png", imagePath, this), null);
            }
            if (ExecutingBounds.isEndNode(node)){
                return SwingFXUtils.toFXImage(Methods.putImageUnderExistImage(imagePath, "images/endBoundImg.png", this), null);
            }
        }catch (IOException exception){
            Log.info("checkIsNodeIsBoundAndChangeImageIfNeed Error: " + exception.getMessage());
        }

        return new Image(Main.class.getResourceAsStream(imagePath));
    }

    private void setExpandedChangeListener(TreeItem<String> tempTreeItem) {
        tempTreeItem.expandedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                BooleanProperty bb = (BooleanProperty) observable;
                TreeItem changedTreeItem = (TreeItem) bb.getBean();
                ExpandMapper.changeOrSetMapItem(Methods.getIndexMapForSelectedTreeItem(changedTreeItem), newValue);
            }
        });
    }

    private void setContextMenuForTreeView(TreeView treeItem){
        ContextMenu contextMenu = new ContextMenu();

        MenuItem delOne = new MenuItem("Удалить");
        delOne.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                delSelectedNodes();
            }
        });
        MenuItem delAll = new MenuItem("Удалить все");
        delAll.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Log.info("Select Delete All function");
                try {
                    if (Methods.twoStateDialog("Удалить все узлы?") == DialogEndStatus.YES){
                       delAllNodes();
                    }
                } catch (IOException e) {
                    Log.info("Main: Delete All context Error"+e.fillInStackTrace());
                }
            }
        });
        MenuItem item_edit = new MenuItem("Изменить");
        item_edit.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Log.info("Select EditFunc for: "+ selectedNode.getNodeName());
                editByTreeSelection(selectedNode);
            }
        });
        MenuItem save = new MenuItem("Сохранить");
        save.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                SaveProgramFile();
            }
        });
        MenuItem copy_item = new MenuItem("Копировать");
        copy_item.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                copySelectedProgramTreeItems2InnerBuffer();
                bufferedElement = selectedNode.cloneNode(true);
            }
        });
        MenuItem copy2buff_item = new MenuItem("Копировать в буфер обмена");
        copy2buff_item.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                copySelectedItems2ClipBoard();
                bufferedElement = selectedNode.cloneNode(true);
            }
        });
        MenuItem pastFromBuffer = new MenuItem("Вставить из буфера обмена");
        pastFromBuffer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    addTempFile();
                    ProgramSaved = false;
                    ClipBoardDrive.getItemsFromClipBoard().stream().forEach(node ->
                            pastBufferedElementAfterSelectedNode(ProgramDocument.adoptNode(node.cloneNode(true))));
                    reloadProgramFromDoc();
                    changeCount++;
                } catch (Exception e) {
                    try {
                        Methods.messageDialog("Не удалось вставить элементы из буфера обмена!");
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        MenuItem cut_item = new MenuItem("Вырезать");
        cut_item.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                copySelectedProgramTreeItems2InnerBuffer();
                addTempFile();
                SelectionProgramBuffer.getNodes().stream().forEach(ProgramObjectClass.this::delOneNode);
                changeCount++;
                reloadProgramFromDoc();
            }
        });
        MenuItem past2item = new MenuItem("Вставить после");
        past2item.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                addTempFile();
                ProgramSaved = false;
                SelectionProgramBuffer.getNodes().stream().forEach(node ->
                        pastBufferedElementAfterSelectedNode(ProgramDocument.adoptNode(node.cloneNode(true))));
                reloadProgramFromDoc();
                changeCount++;
            }
        });
        MenuItem open = new MenuItem("Открыть");
        open.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Log.info("Select open function");
                try {
                    if (ProgramSaved?false:Methods.twoStateDialog("Сохранить текущую программу?")
                            == DialogEndStatus.YES) {
                        //алгоритм сохранения текущей прогрраммы
                        if (SaveProgramFile())
                            MainController.openProgramFile();
                    } else {
                        MainController.openProgramFile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        MenuItem newPrg = new MenuItem("Создать новую программу");
        newPrg.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Log.info("Select Create newPRG function");
                try {
                    if (ProgramSaved?false:Methods.twoStateDialog("Сохранить текущую программу?") == DialogEndStatus.YES) {
                        //алгоритм сохранения текущей прогрраммы
                        if (SaveProgramFile())
                            MainController.initNewProgram();
                    } else {
                        MainController.initNewProgram();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        MenuItem addStartPoint = new MenuItem("Установить/убрать начало запуска");
        addStartPoint.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ExecutingBounds.setExecutePoint(ExecutePointTypes.BEGIN, selectedNode);
                ProgramSaved = false;
                reloadProgramFromDoc();
            }
        });
        MenuItem addStopPoint = new MenuItem("Установить/убрать конец запуска");
        addStopPoint.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ExecutingBounds.setExecutePoint(ExecutePointTypes.END, selectedNode);
                ProgramSaved = false;
                reloadProgramFromDoc();
            }
        });

        contextMenu.getItems().add(delOne);
        contextMenu.getItems().add(delAll);
        contextMenu.getItems().add(item_edit);
        contextMenu.getItems().add(new SeparatorMenuItem());
        contextMenu.getItems().add(save);
        contextMenu.getItems().add(open);
        contextMenu.getItems().add(newPrg);

        treeItem.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {

            @Override
            public void handle(ContextMenuEvent event) {
                contextMenu.setStyle("-fx-background-color: #fff; -fx-background-radius: 5px; -fx-border-color: black; -fx-border-radius: 5px;");
                contextMenu.getItems().clear();
                if (selectedNode == null){
                    return;
                }
                if (selectedNode.getParentNode().getNodeName().equals("result")){
                    contextMenu.getItems().add(delAll);
                    contextMenu.getItems().add(save);
                    contextMenu.getItems().add(open);
                    contextMenu.getItems().add(newPrg);
                }else{
                    contextMenu.getItems().add(addStartPoint);
                    contextMenu.getItems().add(addStopPoint);
                    contextMenu.getItems().add(delOne);
                    contextMenu.getItems().add(delAll);
                    contextMenu.getItems().add(copy_item);
                    contextMenu.getItems().add(cut_item);
                    contextMenu.getItems().add(past2item);
                    contextMenu.getItems().add(new SeparatorMenuItem());
                    contextMenu.getItems().add(copy2buff_item);
                    contextMenu.getItems().add(pastFromBuffer);
                    contextMenu.getItems().add(new SeparatorMenuItem());
                    contextMenu.getItems().add(item_edit);
                    contextMenu.getItems().add(new SeparatorMenuItem());
                    contextMenu.getItems().add(save);
                    contextMenu.getItems().add(open);
                    contextMenu.getItems().add(newPrg);
                }
                contextMenu.show(struct_tree.getScene().getWindow(), event.getScreenX(), event.getScreenY());
            }
        });
    }

    private List<Node> getSelectedItemsList(){
        List<Node> items = new ArrayList<>();
        struct_tree.getSelectionModel().getSelectedItems().stream().filter(item -> item != null)
                .map(Methods::getIndexMapForSelectedTreeItem)
                .map(integers -> Methods.getNodeFromDocumentByIndexMap(ProgramDocument, integers, selfLink))
                .forEach(node -> items.add(node));
        return items;
    }

    private void copySelectedItems2ClipBoard(){
        ClipBoardDrive.sendItemsToClipBoard(getSelectedItemsList());
    }

    private void copySelectedProgramTreeItems2InnerBuffer() {
        SelectionProgramBuffer.clearNodes();
        SelectionProgramBuffer.addAll(getSelectedItemsList());
    }

    public void editSelectedNode(){
        editByTreeSelection(selectedNode);
    }

    private void pastBufferedElementAfterSelectedNode(Node bufferedElement) {
        /**
         * Вставляет скопированый ранее элемент после выделенного ранее (последнего)
         **/
        if (bufferedElement!=null){
            addElement2Doc((Element) bufferedElement, false);
            if (bufferedElement.getNodeName().equals("block")){
                ((Pane) GUIObjects.get("panels").get("workPane")).setVisible(true);
                blockCount++;
            }
            if (bufferedElement.getNodeName().equals("result")){
                if (!checkResultBlockCorrect(bufferedElement)){
                    try {
                        if (Methods.twoStateDialog("Вставляемый блок результата может быть " +
                                "не корректно рассчитан, вставить блок?") != DialogEndStatus.YES){
                            delOneNode(bufferedElement);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else{
            try {
                Methods.messageDialog("Скопируйте елемент предварительно!");
            } catch (IOException e) {
                Log.info(e.getStackTrace().toString());
            }
        }
    }

    private void reWriteResultsIfHaveIt(NodeList searchNodeList) {
        for (int i = 0; i<searchNodeList.getLength();i++){
            Node temp = searchNodeList.item(i);
            if (temp.getNodeType()==Node.ELEMENT_NODE){
                if (temp.getNodeName().equals("result")){
                    Node tempSelected = selectedNode;
                    selectedNode = temp;
                    editResultBlock(temp, true);
                    selectedNode = tempSelected;
                } else if(temp.hasChildNodes()){
                    reWriteResultsIfHaveIt(temp.getChildNodes());
                }
            }
        }
    }

    private void editByTreeSelection(Node selectedNode) {
        addTempFile();
        switch (selectedNode.getNodeName()){
            case "voltageSource": case "voltageSourceReset":
                    editVoltageSource(selectedNode);
                break;
            case "voltMeas":
                    editVoltageMeas(selectedNode);
                break;
            case "currentSource": case "currentSourceReset":
                    editCurrentSource(selectedNode);
                break;
            case "currMeas":
                editCurrentMeas(selectedNode);
                break;
            case "block":
                editBlock(selectedNode);
                break;
            case "param":
                editParameter(selectedNode);
                break;
            case "pause":
                editPause(selectedNode);
                break;
            case "moving":
                editMoving(selectedNode);
                break;
            case "header":
                editHeader(selectedNode);
                break;
            case "result":
                editResultBlock(selectedNode, false);
                break;
            case "Vac":
                editVACBlock(selectedNode);
                break;
            case "d_block":
                editDoubleBlock(selectedNode);
                break;
            default:
                break;
        }
        changeCount++;
    }

    private Node getPrevious(Node selectedNode) {
        Node parent = selectedNode.getParentNode();
        NodeList childes = parent.getChildNodes();
        Node PrevNode = parent;
        for (int i = 0; i<childes.getLength();i++){
            Node temp = childes.item(i);
            if (temp.getNodeType()==Node.ELEMENT_NODE){
                if (temp.equals(selectedNode))
                    break;
                else PrevNode=temp;
            }
        }

        return PrevNode;
    }

    public TreeView<String> readProgramFile(){

        // Дерево проекта программы - определяем корневой узел
        TreeItem<String> rootTreeNode = new TreeItem<String>("Программа");
        rootTreeNode.setExpanded(true);
        // устанавливаем корневой узел для TreeView
        setRoot4TreeView(rootTreeNode);
        TreeItem <String> parentTreeItem = rootTreeNode;
        initGUIObjects();
        try {
            this.ProgramDocument = new workWithXMLStructure().createNewXMLDocument(filePath);
            NodeList nList = new workWithXMLStructure().getNodesFromDocument(ProgramDocument);

            for (int i = 0; i<nList.getLength();i++){
                if (nList.item(i).getNodeType()==Node.ELEMENT_NODE) {
                    selectedNode = nList.item(i);
                    break;
                }
            }

            TreeGenerationFromNodeList(nList, parentTreeItem);
            ProgramSaved = true;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } finally {
            return this.struct_tree;
        }

    }

    private void reloadProgramFromDoc(){
        TreeItem <String> parentTreeItem = this.struct_tree.getRoot();
        ProgramSaved = false;
        try {
            NodeList nList = new workWithXMLStructure().getNodesFromDocument(this.ProgramDocument);
            //Map<TreeItem, Boolean> expandedMap = Methods.getExpandedMapFromTreeView(struct_tree);
            TreeGenerationFromNodeList(nList, parentTreeItem);
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        }

    }


    TreeView<String> createNewProgram(){
        try {
            this.ProgramDocument = new workWithXMLStructure().createNewXMLDocument();
            Element root = this.ProgramDocument.createElement("prg");
            root.setAttribute("name", (this.prgName == "" ? "Новая программа" : this.prgName));
            this.ProgramDocument.appendChild(root);
        }catch (Exception e){Log.info("ProgramObject: createNewProgram Error: "+e.fillInStackTrace());}

        TreeItem<String> rootTreeNode = new TreeItem<String>((this.prgName == "" ? "Новая программа" : this.prgName));
        rootTreeNode.setExpanded(true);
        // устанавливаем корневой узел для TreeView
        setRoot4TreeView(rootTreeNode);
        return this.struct_tree;
    }

    private void setRoot4TreeView(TreeItem<String> rootTreeNode) {
        struct_tree = new TreeView<String>(rootTreeNode);
        setContextMenuForTreeView(struct_tree);
        struct_tree.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        struct_tree.setMinHeight(750.0);
        struct_tree.setMinWidth(350.0);
    }

    private TreeItem<String> getLastFromTree(TreeItem<String> item){
        Iterator<TreeItem<String>> iter = item.getChildren().listIterator();
        TreeItem<String> last = item;
        while (iter.hasNext()) {
            //Log.info(count--);
            last = iter.next();
            if (!iter.hasNext()) {
                try {
                    last = getLastFromTree(last.getChildren().listIterator().next());
                } catch (NoSuchElementException e) {
                    Log.info(e.getLocalizedMessage());
                    return last;
                }
                break;
            }
        }
        return last;
    }

    private Element getLastFromDoc(Node node){
        if (selectedNode!=null){
            if (selectedNode.getNodeName().equals("d_block")){
                Element newEl = this.ProgramDocument.createElement("block");
                //Наполенение элемента
                newEl.setAttribute("id",Integer.toString(blockCount));
                newEl.setAttribute("name","Блок");
                insertNodeAfterChildNode((Element) selectedNode,newEl);
                blockCount++;
                selectedNode =  newEl;
            }
            return (Element) selectedNode;
        }
        else return (Element) this.ProgramDocument.getDocumentElement();
    }

    private Element getLastFromDoc(Node node, String tagName){
        //последний элемент с конкретным типом (tegName)
        Boolean findElem = false;
        if (node.hasChildNodes()){
            NodeList nList = node.getChildNodes();

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node tNode = nList.item(temp);
                if (tNode.getNodeType() == Node.ELEMENT_NODE && tNode.getNodeName().equals(tagName)) {
                    node = tNode;
                    findElem = true;
                }
            }
        }
        if (findElem)
            return (node.hasChildNodes()?getLastFromDoc(node):(Element)node);
        else return (Element)node;
        //return (item.hasChildNodes()? getLastFromDoc(item.getLastChild()):item);
    }

    public void addRelay(Relay r_0_0) {
        try {
            Element newEl = this.ProgramDocument.createElement("switch");
            //Наполенение элемента
            newEl.setAttribute("id", r_0_0.getRelay_id());
            newEl.setAttribute("status", (r_0_0.getRelay_status()?"1":"0"));
            newEl.setTextContent(r_0_0.displayInfo());
            addElement2Doc(newEl);
        }catch (NullPointerException e){
            Log.info("ProgramObject: addRelay Error"+e.fillInStackTrace());
        }

    }
    public void addBlock(String blockName){
        Element newEl = this.ProgramDocument.createElement("block");
        //Наполенение элемента
        newEl.setAttribute("id",Integer.toString(blockCount));
        newEl.setAttribute("name", blockName);
        addElement2Doc(newEl);
        blockCount++;
    }

    private void editBlock(Node selectedNode){
        try {
            String name = selectedNode.getAttributes().getNamedItem("name").getNodeValue();
            String newName = Methods.twoStateDialogWithTextFieldDef("Изменить блок", "Имя блока:", name,"string");
            selectedNode.getAttributes().getNamedItem("name").setNodeValue(newName);
            Methods.reWriteLinkedDoubleBlocksForBlock(ProgramDocument, selectedNode, name, newName);
            reloadProgramFromDoc();
        } catch (IOException e) {
            Log.info("Edit Block error: "+e.getMessage());
        }
    }
    public void addParameter(String blockName) {
        Element newEl = this.ProgramDocument.createElement("param");
        //Наполенение элемента
        newEl.setAttribute("name", blockName);
        addElement2Doc(newEl);
    }
    private void editParameter(Node selectedNode){
        try {
            String name = selectedNode.getAttributes().getNamedItem("name").getNodeValue();
            String newName = Methods.twoStateDialogWithTextFieldDef("Изменить параметр", "Имя параметра:", name,"string");
            selectedNode.getAttributes().getNamedItem("name").setNodeValue(newName);
            reloadProgramFromDoc();
        } catch (IOException e) {
            Log.info("Edit parameter error: "+e.getMessage());
        }
    }
    public void addFullReset() {
        Element newEl = this.ProgramDocument.createElement("fullReset");
        addElement2Doc(newEl);
    }
    public void addPause(String pause) {
        Element newEl = this.ProgramDocument.createElement("pause");
        //Наполенение элемента
        newEl.setAttribute("value", pause);
        addElement2Doc(newEl);
    }
    private void editPause(Node selectedNode){
        try {
            String value = selectedNode.getAttributes().getNamedItem("value").getNodeValue();
            String newValue = Methods.twoStateDialogWithTextFieldDef("Изменить паузу", "Пауза, мс:", value,"int");
            selectedNode.getAttributes().getNamedItem("value").setNodeValue(newValue);
            reloadProgramFromDoc();
        } catch (IOException e) {
            Log.info("Edit pause error: "+e.getMessage());
        }
    }
    public void addVoltageSource(String sourceID, Map<String, String> values) {
        try{
            if (values.get("status").equals("voltage")){
                //задание напряжения
                Element newEl = this.ProgramDocument.createElement("voltageSource");
                //Наполенение элемента
                newEl.setAttribute("id", sourceID);
                newEl.setAttribute("voltage", values.get("voltage"));
                newEl.setAttribute("voltageRange", values.get("range"));
                newEl.setAttribute("currentLimit", values.get("currentLim"));
                newEl.setAttribute("currentLimitRange", values.get("currentLimRange"));
                newEl.setAttribute("comment", values.get("comment"));
                newEl.setAttribute("timeMode", values.getOrDefault("timeMode",
                        SourceTimeMode.NORMAL.name()));
                addElement2Doc(newEl);
            }else{
                //сброс источника
                Element newEl = this.ProgramDocument.createElement("voltageSourceReset");
                //Наполенение элемента
                newEl.setAttribute("id", sourceID);
                newEl.setAttribute("comment", values.get("comment"));
                addElement2Doc(newEl);
            }
        }catch (Exception e){
            Log.info("addVoltageSource Error: "+e.fillInStackTrace());
        }
    }
    public void editVoltageSource(Node SelectedNode) {
        //get data from node
        Map<String, String> values = new HashMap<String, String>();
        if (SelectedNode.getNodeName().equals("voltageSource")) {
            values.put("status", "voltage");
            values.put("voltage", selectedNode.getAttributes().getNamedItem("voltage").getNodeValue());
            values.put("range", selectedNode.getAttributes().getNamedItem("voltageRange").getNodeValue());
            values.put("currentLim", selectedNode.getAttributes().getNamedItem("currentLimit").getNodeValue());
            values.put("currentLimRange", selectedNode.getAttributes().getNamedItem("currentLimitRange").getNodeValue());
            try {
                values.put("timeMode", selectedNode.getAttributes().getNamedItem("timeMode").getNodeValue());
            }catch (Exception exception){
                values.put("timeMode", SourceTimeMode.NORMAL.name());
            }
        }else values.put("status", "reset");
        values.put("comment", selectedNode.getAttributes().getNamedItem("comment").getNodeValue());
        //load to voltageSourceController
        try {
            Stage dialog = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/voltSource.fxml"));
            Parent root = loader.load();

            dialog.setScene(new Scene(root));

            dialog.initModality(Modality.APPLICATION_MODAL);
            voltSourceController controller = loader.<voltSourceController>getController();
            PowerSource I = (PowerSource) GUIObjects.get("powerSources").get(selectedNode.getAttributes().getNamedItem("id").getNodeValue());
            controller.setSource(I);
            controller.setValues(values);
            dialog.showAndWait();
            //получаем статус завершения диалога с пользователем
            if(controller.getDialog_status().equals("yes")){
                if (values.get("status").equals("voltage")){
                    //задание напряжения
                    Element newEl = this.ProgramDocument.createElement("voltageSource");
                    //Наполенение элемента
                    newEl.setAttribute("id", I.sourceId);
                    newEl.setAttribute("voltage", values.get("voltage"));
                    newEl.setAttribute("voltageRange", values.get("range"));
                    newEl.setAttribute("currentLimit", values.get("currentLim"));
                    newEl.setAttribute("currentLimitRange", values.get("currentLimRange"));
                    newEl.setAttribute("comment", values.get("comment"));
                    newEl.setAttribute("timeMode", values.getOrDefault("timeMode",
                            SourceTimeMode.NORMAL.name()));
                    selectedNode.getParentNode().replaceChild(newEl,selectedNode);
                }else{
                    //сброс источника
                    Element newEl = this.ProgramDocument.createElement("voltageSourceReset");
                    //Наполенение элемента
                    newEl.setAttribute("id", I.sourceId);
                    newEl.setAttribute("comment", values.get("comment"));
                    selectedNode.getParentNode().replaceChild(newEl,selectedNode);
                }
                reloadProgramFromDoc();
            }
        }catch (IOException e){
            Log.info("Edit Voltage Source:"+e.fillInStackTrace());
        }
    }
    public void addVoltageMeas(String sourceId, Map<String, String> values) {
        Element newEl = this.ProgramDocument.createElement("voltMeas");
        newEl.setAttribute("id", sourceId);
        newEl.setAttribute("voltageRange", values.get("range"));
        //newEl.setAttribute("measID", Integer.toString(getMeasCount()));
        addElement2Doc(newEl);
    }
    private void editVoltageMeas(Node selectedNode){
        String range = selectedNode.getAttributes().getNamedItem("voltageRange").getNodeValue();
        try {
            Stage dialog = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/measVolt.fxml"));
            Parent root = loader.load();
            PowerSource I = (PowerSource) GUIObjects.get("powerSources").get(selectedNode.getAttributes().getNamedItem("id").getNodeValue());
            dialog.setScene(new Scene(root));

            dialog.initModality(Modality.APPLICATION_MODAL);
            voltMeasController controller = loader.<voltMeasController>getController();
            controller.setSource(I);
            controller.setRange(range);
            dialog.showAndWait();
            //получаем статус завершения диалога с пользователем
            if(controller.getDialog_status().equals("yes")){
                selectedNode.getAttributes().getNamedItem("voltageRange").setNodeValue(controller.getValues().get("range"));
                reloadProgramFromDoc();
            }
        }catch (IOException e){
            Log.info("Edit VoltageMeas Error "+selectedNode.getAttributes().getNamedItem("id")+": "+e.fillInStackTrace());
        }
    }
    public void addCurrentMeas(String sourceId, Map<String, String> values) {
        Element newEl = this.ProgramDocument.createElement("currMeas");
        newEl.setAttribute("id", sourceId);
        newEl.setAttribute("currentRange", values.get("range"));
        //newEl.setAttribute("measID", Integer.toString(getMeasCount()));
        addElement2Doc(newEl);
    }
    private void editCurrentMeas(Node selectedNode){
        String range = selectedNode.getAttributes().getNamedItem("currentRange").getNodeValue();
        try {
            Stage dialog = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/measCurr.fxml"));
            Parent root = loader.load();
            PowerSource I = (PowerSource) GUIObjects.get("powerSources").get(selectedNode.getAttributes().getNamedItem("id").getNodeValue());
            dialog.setScene(new Scene(root));

            dialog.initModality(Modality.APPLICATION_MODAL);
            currMeasController controller = loader.<currMeasController>getController();
            controller.setSource(I);
            controller.setRange(range);
            dialog.showAndWait();
            //получаем статус завершения диалога с пользователем
            if(controller.getDialog_status().equals("yes")){
                selectedNode.getAttributes().getNamedItem("currentRange").setNodeValue(controller.getValues().get("range"));
                reloadProgramFromDoc();
            }
        }catch (IOException e){
            Log.info("Edit CurrentMeas Error "+selectedNode.getAttributes().getNamedItem("id")+": "+e.fillInStackTrace());
        }
    }

    public void addCurrentSource(String sourceID, Map<String, String> values) {
        try{
            if (values.get("status").equals("current")){
                //задание напряжения
                Element newEl = this.ProgramDocument.createElement("currentSource");
                //Наполенение элемента
                newEl.setAttribute("id", sourceID);
                newEl.setAttribute("current", values.get("current"));
                newEl.setAttribute("currentRange", values.get("range"));
                newEl.setAttribute("voltageLimit", values.get("voltageLim"));
                newEl.setAttribute("voltageLimitRange", values.get("voltageLimRange"));
                newEl.setAttribute("comment", values.get("comment"));
                newEl.setAttribute("timeMode", values.getOrDefault("timeMode",
                        SourceTimeMode.NORMAL.name()));
                addElement2Doc(newEl);
            }else{
                //сброс источника
                Element newEl = this.ProgramDocument.createElement("currentSourceReset");
                //Наполенение элемента
                newEl.setAttribute("id", sourceID);
                newEl.setAttribute("comment", values.get("comment"));
                addElement2Doc(newEl);
            }
        }catch (Exception e){
            Log.info("addCurrentSource Error: "+e.fillInStackTrace());
        }
    }

    public void editCurrentSource(Node SelectedNode) {
        //get data from node
        Map<String, String> values = new HashMap<String, String>();
        if (SelectedNode.getNodeName().equals("currentSource")) {
            values.put("status", "current");
            values.put("current", selectedNode.getAttributes().getNamedItem("current").getNodeValue());
            values.put("range", selectedNode.getAttributes().getNamedItem("currentRange").getNodeValue());
            values.put("voltageLim", selectedNode.getAttributes().getNamedItem("voltageLimit").getNodeValue());
            values.put("voltageLimRange", selectedNode.getAttributes().getNamedItem("voltageLimitRange").getNodeValue());
            try {
                values.put("timeMode", selectedNode.getAttributes().getNamedItem("timeMode").getNodeValue());
            }catch (Exception exception){
                values.put("timeMode", SourceTimeMode.NORMAL.name());
            }
        }else values.put("status", "reset");
        values.put("comment", selectedNode.getAttributes().getNamedItem("comment").getNodeValue());
        //load to currentSourceController
        try {
            Stage dialog = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/currSource.fxml"));
            Parent root = loader.load();

            dialog.setScene(new Scene(root));

            dialog.initModality(Modality.APPLICATION_MODAL);
            currSourceController controller = loader.<currSourceController>getController();
            PowerSource I = (PowerSource) GUIObjects.get("powerSources").get(selectedNode.getAttributes().getNamedItem("id").getNodeValue());
            controller.setSource(I);
            controller.setValues(values);
            dialog.showAndWait();
            //получаем статус завершения диалога с пользователем
            if(controller.getDialog_status().equals("yes")){
                if (values.get("status").equals("current")){
                    //задание напряжения
                    Element newEl = this.ProgramDocument.createElement("currentSource");
                    //Наполенение элемента
                    newEl.setAttribute("id", I.sourceId);
                    newEl.setAttribute("current", values.get("current"));
                    newEl.setAttribute("currentRange", values.get("range"));
                    newEl.setAttribute("voltageLimit", values.get("voltageLim"));
                    newEl.setAttribute("voltageLimitRange", values.get("voltageLimRange"));
                    newEl.setAttribute("comment", values.get("comment"));
                    newEl.setAttribute("timeMode", values.getOrDefault("timeMode",
                            SourceTimeMode.NORMAL.name()));
                    selectedNode.getParentNode().replaceChild(newEl,selectedNode);
                }else{
                    //сброс источника
                    Element newEl = this.ProgramDocument.createElement("currentSourceReset");
                    //Наполенение элемента
                    newEl.setAttribute("id", I.sourceId);
                    newEl.setAttribute("comment", values.get("comment"));
                    selectedNode.getParentNode().replaceChild(newEl,selectedNode);
                }
                reloadProgramFromDoc();
            }
        }catch (IOException e){
            Log.info("Edit Current Source:"+e.fillInStackTrace());
        }
    }

    public void addDoubleBlock(String name, String prototype) {
        Element newEl = this.ProgramDocument.createElement("d_block");
        newEl.setAttribute("name", name);
        //prototype.split(";");
        newEl.setAttribute("pro_id", prototype.split(";")[0]);
        newEl.setAttribute("pro_name", prototype.split(";")[1]);
        addElement2Doc(newEl);
    }
    public void editDoubleBlock(Node selectedNode){
        try {
            String name = selectedNode.getAttributes().getNamedItem("name").getNodeValue();
            String newName = Methods.twoStateDialogWithTextFieldDef("Изменить блок", "Имя блока:", name, "string");
            selectedNode.getAttributes().getNamedItem("name").setNodeValue(newName);
            reloadProgramFromDoc();
        }catch (Exception exception){
            Log.info("editDoubleBlock Error" + exception.getMessage());
        }
    }
    public void addMoving(String movingStr) {
        String[] params = movingStr.split(";");
        Element newEl = this.ProgramDocument.createElement("moving");
        newEl.setAttribute("Type", params[0]);
        newEl.setAttribute("x", params[1]);
        newEl.setAttribute("y", params[2]);
        newEl.setAttribute("unit", params[3]);
        addElement2Doc(newEl);
    }
    private void editMoving(Node selectedNode){
        String moving = selectedNode.getAttributes().getNamedItem("Type").getNodeValue()+";"+
                selectedNode.getAttributes().getNamedItem("x").getNodeValue()+";"+
                selectedNode.getAttributes().getNamedItem("y").getNodeValue()+";"+
                selectedNode.getAttributes().getNamedItem("unit").getNodeValue();
        try {
            Stage dialog = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/moving.fxml"));
            Parent root = loader.load();

            dialog.setScene(new Scene(root));

            dialog.initModality(Modality.APPLICATION_MODAL);
            MovingController controller = loader.<MovingController>getController();
            controller.setMoving(moving);
            dialog.showAndWait();

            if(controller.getDialog_status().equals("yes")){
                String[] params = controller.getMoving().split(";");
                selectedNode.getAttributes().getNamedItem("Type").setNodeValue(params[0]);
                selectedNode.getAttributes().getNamedItem("x").setNodeValue(params[1]);
                selectedNode.getAttributes().getNamedItem("y").setNodeValue(params[2]);
                selectedNode.getAttributes().getNamedItem("unit").setNodeValue(params[3]);
                reloadProgramFromDoc();
            }
        }catch (IOException e){
            Log.info("Edit Error moving"+e.getLocalizedMessage());
        }
    }
    public void addHeader(String blockName) {
        Element newEl = this.ProgramDocument.createElement("header");
        newEl.setTextContent(blockName);
        addElement2Doc(newEl);
    }
    private void editHeader(Node selectedNode){
        try {
            String name = selectedNode.getTextContent();
            String newName = Methods.twoStateDialogWithTextFieldDef("Изменить заголовок", "Заголовок:", name,"string");
            selectedNode.setTextContent(newName);
            reloadProgramFromDoc();
        } catch (IOException e) {
            Log.info("Edit Header error: "+e.getMessage());
        }
    }
    public void addResultBlock(HashMap<String,String> values){
        boolean isNormed = values.getOrDefault("normed", "-").equals("+");

        Element newEl = this.ProgramDocument.createElement("result");
        newEl.setAttribute("Exp",values.get("Exp"));
        newEl.setAttribute("IndExp",values.get("IndExp"));
        newEl.setAttribute("measCount",values.get("measCount"));
        newEl.setAttribute("minNorm",values.get("minNorm"));
        newEl.setAttribute("maxNorm",values.get("maxNorm"));
        newEl.setAttribute("unit",values.get("unit"));
        newEl.setAttribute("normed",isNormed?"+":"-");
        newEl.setAttribute("afterDigitCount",values.get("afterDigitCount"));

        Element newSubEl = this.ProgramDocument.createElement("exp");
        newSubEl.setTextContent(values.get("Exp"));
        newEl.appendChild(newSubEl);
        Element newSubEl1 = this.ProgramDocument.createElement("unit");
        newSubEl1.setTextContent(values.get("unit"));
        newEl.appendChild(newSubEl1);
        if(isNormed) {
            Element newSubEl2 = this.ProgramDocument.createElement("minNorm");
            newSubEl2.setTextContent(values.get("minNorm"));
            newEl.appendChild(newSubEl2);
            Element newSubEl3 = this.ProgramDocument.createElement("maxNorm");
            newSubEl3.setTextContent(values.get("maxNorm"));
            newEl.appendChild(newSubEl3);
        }
        addElement2Doc(newEl);
    }
    private boolean editResultBlock(Node selectedNode, boolean reload){
        boolean execStatus = true;
        ArrayList<Node> measNodes = getMeasNodes();
        String expression = selectedNode.getAttributes().getNamedItem("Exp").getNodeValue();
        if (reload){
            try {
                String newIndexExp = ResultReIndexMaker.GetIndexedExpString(expression, measNodes);
                selectedNode.getAttributes().getNamedItem("IndExp").setNodeValue(newIndexExp);
            }catch (IllegalArgumentException indexedError){
                execStatus = false;
            }
            return execStatus;
        }
        String indexedExp = selectedNode.getAttributes().getNamedItem("IndExp").getNodeValue();
        Map<String, String> values = new HashMap<String, String>();
        values.put("IndExp", indexedExp);
        values.put("Exp", expression);
        values.put("measCount", selectedNode.getAttributes().getNamedItem("measCount").getNodeValue());
        values.put("minNorm",selectedNode.getAttributes().getNamedItem("minNorm").getNodeValue());
        values.put("maxNorm",selectedNode.getAttributes().getNamedItem("maxNorm").getNodeValue());
        values.put("unit", selectedNode.getAttributes().getNamedItem("unit").getNodeValue());

        try {
            String afterPointDigitCount = selectedNode.getAttributes().getNamedItem("afterDigitCount").getNodeValue();
            values.put("afterDigitCount", afterPointDigitCount);
        }catch (NullPointerException nullPointerException){}
        try {
            String normed = selectedNode.getAttributes().getNamedItem("normed").getNodeValue();
            values.put("normed", normed);
        }catch (NullPointerException nullPointerException){
            boolean isNormed = !values.get("minNorm").equals("-") && !values.get("maxNorm").equals("-");
            values.put("normed", isNormed?"+":"-");
        }
        try{
            Stage dialog = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/result.fxml"));
            Parent root = loader.load();
            dialog.setScene(new Scene(root));
            dialog.initModality(Modality.APPLICATION_MODAL);
            ResultController controller = loader.<ResultController>getController();
            controller.sendMeasListData(measNodes);
            controller.setValues(values);
            dialog.showAndWait();
            if(controller.getDialog_status()==DialogEndStatus.YES) {
                values = controller.getOutValues();
                boolean isNormed = values.getOrDefault("normed", "-").equals("+");
                Element newEl = this.ProgramDocument.createElement("result");
                newEl.setAttribute("Exp",values.get("Exp"));
                newEl.setAttribute("IndExp",values.get("IndExp"));
                newEl.setAttribute("measCount",values.get("measCount"));
                newEl.setAttribute("minNorm",values.get("minNorm"));
                newEl.setAttribute("maxNorm",values.get("maxNorm"));
                newEl.setAttribute("unit",values.get("unit"));
                newEl.setAttribute("afterDigitCount",values.get("afterDigitCount"));
                newEl.setAttribute("normed",isNormed?"+":"-");

                Element newSubEl = this.ProgramDocument.createElement("exp");
                newSubEl.setTextContent(values.get("Exp"));
                newEl.appendChild(newSubEl);
                Element newSubEl1 = this.ProgramDocument.createElement("unit");
                newSubEl1.setTextContent(values.get("unit"));
                newEl.appendChild(newSubEl1);
                if(isNormed) {
                    Element newSubEl2 = this.ProgramDocument.createElement("minNorm");
                    newSubEl2.setTextContent(values.get("minNorm"));
                    newEl.appendChild(newSubEl2);
                    Element newSubEl3 = this.ProgramDocument.createElement("maxNorm");
                    newSubEl3.setTextContent(values.get("maxNorm"));
                    newEl.appendChild(newSubEl3);
                }
                selectedNode.getParentNode().replaceChild(newEl, selectedNode);
                reloadProgramFromDoc();
            }
        }catch (IOException e){
            Log.info("GUI Error dataInputButt"+e.getLocalizedMessage());
        }
        return execStatus;
    }

    private boolean checkResultBlockCorrect(Node result){
            return  editResultBlock(result, true);
    }

    public Element addVACBlock(HashMap<String, String> outValues, boolean add) {
        Element newEl = this.ProgramDocument.createElement("Vac");
        newEl.setAttribute("sourceID", outValues.get("sourceID"));
        newEl.setAttribute("sourceStartValue", outValues.get("sourceStartValue"));
        newEl.setAttribute("sourceEndValue", outValues.get("sourceEndValue"));
        newEl.setAttribute("sourceStep", outValues.get("sourceStep"));
        newEl.setAttribute("plotGainX", outValues.get("plotGainX"));
        newEl.setAttribute("labelX", outValues.get("labelX"));
        newEl.setAttribute("unitX", outValues.get("unitX"));
        newEl.setAttribute("measureID", outValues.get("measureID"));
        newEl.setAttribute("plotGainY", outValues.get("plotGainY"));
        newEl.setAttribute("labelY", outValues.get("labelY"));
        newEl.setAttribute("unitY", outValues.get("unitY"));
        newEl.setAttribute("measureDelay", outValues.get("measureDelay"));
        newEl.setAttribute("needPlot", outValues.get("needPlot"));

        Element newSubEl = this.ProgramDocument.createElement("VacSource");
        newSubEl.setTextContent("Источник "+outValues.get("sourceID")+ " // "+outValues.get("labelX"));
        newEl.appendChild(newSubEl);

        Element newSubEl1 = this.ProgramDocument.createElement("VacSourceRange");
        newSubEl1.setTextContent(outValues.get("sourceStartValue")+"=>"+outValues.get("sourceEndValue")+ " by "+outValues.get("sourceStep"));
        newEl.appendChild(newSubEl1);

        Element newSubEl2 = this.ProgramDocument.createElement("VacPlotGainX");
        newSubEl2.setTextContent(outValues.get("plotGainX"));
        newEl.appendChild(newSubEl2);

        Element newSubEl3 = this.ProgramDocument.createElement("VacUnitX");
        newSubEl3.setTextContent(outValues.get("unitX"));
        newEl.appendChild(newSubEl3);

        Element newSubEl4 = this.ProgramDocument.createElement("VacMeasure");
        newSubEl4.setTextContent("Измеритель "+outValues.get("measureID")+ " // "+outValues.get("labelY"));
        newEl.appendChild(newSubEl4);

        Element newSubEl5 = this.ProgramDocument.createElement("VacPlotGainY");
        newSubEl5.setTextContent(outValues.get("plotGainY"));
        newEl.appendChild(newSubEl5);

        Element newSubEl6 = this.ProgramDocument.createElement("VacUnitY");
        newSubEl6.setTextContent(outValues.get("unitY"));
        newEl.appendChild(newSubEl6);

        Element newSubEl7 = this.ProgramDocument.createElement("VacMeasureDelay");
        newSubEl7.setTextContent(outValues.get("measureDelay"));
        newEl.appendChild(newSubEl7);
        if (add)
            addElement2Doc(newEl);
        return newEl;
    }
    private void editVACBlock(Node selectedNode){
        HashMap<String, String> values = new HashMap<String, String>();
        values.put("sourceID",selectedNode.getAttributes().getNamedItem("sourceID").getNodeValue());
        values.put("sourceStartValue",selectedNode.getAttributes().getNamedItem("sourceStartValue").getNodeValue());
        values.put("sourceEndValue",selectedNode.getAttributes().getNamedItem("sourceEndValue").getNodeValue());
        values.put("sourceStep",selectedNode.getAttributes().getNamedItem("sourceStep").getNodeValue());
        values.put("plotGainX",selectedNode.getAttributes().getNamedItem("plotGainX").getNodeValue());
        values.put("labelX",selectedNode.getAttributes().getNamedItem("labelX").getNodeValue());
        values.put("unitX",selectedNode.getAttributes().getNamedItem("unitX").getNodeValue());
        values.put("measureID",selectedNode.getAttributes().getNamedItem("measureID").getNodeValue());
        values.put("plotGainY",selectedNode.getAttributes().getNamedItem("plotGainY").getNodeValue());
        values.put("labelY",selectedNode.getAttributes().getNamedItem("labelY").getNodeValue());
        values.put("unitY",selectedNode.getAttributes().getNamedItem("unitY").getNodeValue());
        values.put("measureDelay",selectedNode.getAttributes().getNamedItem("measureDelay").getNodeValue());
        values.put("needPlot",selectedNode.getAttributes().getNamedItem("needPlot").getNodeValue());
        try {
            Stage dialog = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/VAC.fxml"));
            Parent root = loader.load();
            dialog.setScene(new Scene(root));
            dialog.initModality(Modality.APPLICATION_MODAL);
            VACController controller = loader.<VACController>getController();
            controller.sendSources(GUIObjects.get("powerSources").keySet().toArray());
            controller.setValues(values);
            dialog.showAndWait();

            if (controller.getDialog_status().equals("yes")){
                Element newEl = addVACBlock(controller.getOutValues(), false);
                selectedNode.getParentNode().replaceChild(newEl, selectedNode);
                reloadProgramFromDoc();
            }
        } catch (IOException e) {
            Log.info("GUI Error VACBlock" + e.getLocalizedMessage());
        }
    }

    public String setSelectedNodeByIndexMapAndReturnByString(ArrayList<Integer> indexMap) {
        initGUIObjects();
        String result = "";
        try {
            Node currNode = Methods.getNodeFromDocumentByIndexMap(ProgramDocument, indexMap, this);
            result = getStringForTreeFromNode(currNode);
            this.selectedNode = currNode;
            refreshGUIObjectsByNode(currNode, false);
            //индикация выделеного элемента на схеме
            indicateOnGUI(selectedNode);

        }catch (Exception e){
            Log.info(e.getLocalizedMessage());
        }
        return result;
    }
    private void indicateOnGUI(Node selectedNode) {
        Label indicateName = (Label) GUIObjects.get("panels").get("indicateName");
        Label indicateStatus = (Label) GUIObjects.get("panels").get("indicateStatus");
        switch (selectedNode.getNodeName()){
            case "voltageSource": case "voltMeas": case "currentSource": case "currMeas":
                PowerSource source = (PowerSource) GUIObjects.get("powerSources").get(selectedNode.getAttributes().getNamedItem("id").getNodeValue());
                for(Map.Entry entry:  GUIObjects.get("powerSources").entrySet()) {
                    PowerSource S = (PowerSource) entry.getValue();
                    S.visibleSelectionIcon(source.equals(S), selectedNode.getNodeName());
                }
                source.visibleSelectionIcon(true,selectedNode.getNodeName());
                switch (selectedNode.getNodeName()){
                    case "voltageSource":
                        indicateName.setText("Источник напряжения "+selectedNode.getAttributes().getNamedItem("id").getNodeValue());
                        indicateStatus.setText("Напряжение: "+selectedNode.getAttributes().getNamedItem("voltage").getNodeValue()+";\n"+
                                "Диапазон напряжения: "+selectedNode.getAttributes().getNamedItem("voltageRange").getNodeValue()+";\n"+
                                "Ограничение тока: "+selectedNode.getAttributes().getNamedItem("currentLimit").getNodeValue()+";");
                        break;
                    case "voltMeas":
                        indicateName.setText("Измеритель напряжения "+selectedNode.getAttributes().getNamedItem("id").getNodeValue());
                        indicateStatus.setText("Диапазон напряжения: "+selectedNode.getAttributes().getNamedItem("voltageRange").getNodeValue()+";");
                        break;
                    case "currentSource":
                        indicateName.setText("Источник тока "+selectedNode.getAttributes().getNamedItem("id").getNodeValue());
                        indicateStatus.setText("Ток: "+selectedNode.getAttributes().getNamedItem("current").getNodeValue()+";\n"+
                                "Диапазон тока: "+selectedNode.getAttributes().getNamedItem("currentRange").getNodeValue()+";\n"+
                                "Ограничение напряжения: "+selectedNode.getAttributes().getNamedItem("voltageLimit").getNodeValue()+";");
                        break;
                    case "currMeas":
                        indicateName.setText("Измеритель тока "+selectedNode.getAttributes().getNamedItem("id").getNodeValue());
                        indicateStatus.setText("Диапазон тока: "+selectedNode.getAttributes().getNamedItem("currentRange").getNodeValue()+";");
                        break;
                }
                indicateName.setVisible(true);
                indicateStatus.setVisible(true);
                break;
            case "switch":
                Relay r = (Relay) GUIObjects.get("relays").get(selectedNode.getAttributes().getNamedItem("id").getNodeValue());
                for(Map.Entry entry:  GUIObjects.get("relays").entrySet()) {
                    Relay S = (Relay) entry.getValue();
                    S.visibleSelectionIcon(r.equals(S));
                }
                r.visibleSelectionIcon(true);
                indicateName.setVisible(false);
                indicateStatus.setVisible(false);
                break;
            default:
                indicateName.setVisible(false);
                indicateStatus.setVisible(false);
        }
    }

    boolean checkHierarchyPriority(Element el){
        if (el.getTagName().equals("block")||el.getTagName().equals("param"))
            return true;
        else return false;
    }

    void insertNodeAfterChildNode(Element child, Element newNode){
        try{
            NodeList nList = child.getParentNode().getChildNodes();
            int count = 0;
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node node = nList.item(temp);

                if (node.getNodeType() == Node.ELEMENT_NODE || node.getNodeType() == Node.TEXT_NODE) {
                    if(nList.item(count).equals(child)) break;
                    count++;
                }
            }
            child.getParentNode().insertBefore(newNode, nList.item(count+1));
        }catch (Exception e){
            try {
                child.getParentNode().appendChild(newNode);
            }catch (Exception d){
                child.appendChild(newNode);
            }
            Log.info("Error ProgramObject insertNodeAfterChildNode: "+e.fillInStackTrace());
        }
    }

    private void addElement2Doc(Element newEl, boolean needTempFileMaking){
        if (needTempFileMaking){
            addTempFile();
            ProgramSaved = false;
        }
        Element root = this.ProgramDocument.getDocumentElement();
        String addingMes = "";
        DialogEndStatus dialogEndStatus = DialogEndStatus.CLOSE;
        try {
            Element n = (Element) selectedNode;//(newEl.getTagName().equals("d_block")?((Element) selectedNode):(Element)getLastFromDoc(root));
            switch (newEl.getTagName()) {
                case "block": case "d_block":
                    //1. Найти блок которому принадлежит выделеный сейчас элемент
                    if (getBlockCount()==0) {
                        root.appendChild(newEl);
                    }
                    else{
                        Element upBlock = (Element)((selectedNode.getNodeName().equals("block")
                                || selectedNode.getNodeName().equals("d_block"))
                                    ? selectedNode : getParentElementByName(selectedNode, "block"));
                        insertNodeAfterChildNode(upBlock,newEl);
                    }
                    break;
                case "param":case "header":
                    NodeList blocks = this.ProgramDocument.getElementsByTagName("block");
                    if (blocks.getLength()==0){
                        changeCount++;
                        addBlock("Блок");
                        changeCount--;
                        blocks = this.ProgramDocument.getElementsByTagName("block");
                    }
                    if (selectedNode.getNodeName().equals("block")){
                        try{
                            selectedNode.insertBefore(newEl,selectedNode.getFirstChild());
                        }catch (Exception e){selectedNode.appendChild(newEl);}
                        break;
                    }
                    if (selectedNode.getNodeName().equals("header")){
                        insertNodeAfterChildNode((Element) selectedNode,newEl);
                        break;
                    }
                    boolean insertAfterSelected = selectedNode.getNodeName().equals("param") || selectedNode.getNodeName().equals("moving");
                    Element upBlock = (Element)(insertAfterSelected?selectedNode:getParentElementByName(selectedNode, "param"));
                    insertNodeAfterChildNode(upBlock,newEl);
                    //Element lastBlock = (Element)(selectedNode.getNodeName().equals("block")?selectedNode: blocks.item(blocks.getLength()-1));
                    //lastBlock.appendChild(newEl);
                    break;
                case "pause":
                    addingMes = "паузу";
                case "fullReset":
                    if (addingMes.equals(""))
                        addingMes = "общий сброс";
                case "moving":
                    if (addingMes.equals(""))
                        addingMes = "перемещение";
                    if (n!=null){
                        boolean needQuestion = false;
                        if (n.getTagName().equals("block")){
                            needQuestion = true;
                            dialogEndStatus = twoStateDialog("Добавить "+addingMes+" на уровне блоков (Да) или на уровне параметров(Нет)?");
                        }
                        if (n.getTagName().equals("param")){
                            needQuestion = true;
                            dialogEndStatus = twoStateDialog("Добавить "+addingMes+" на уровне параметров (Да) или на уровне разовых действий(Нет)?");
                        }
                        if (dialogEndStatus == DialogEndStatus.YES || !needQuestion) {
                            insertNodeAfterChildNode(n, newEl);
                            break;
                        }
                        else if (dialogEndStatus == DialogEndStatus.NO){
                            try {
                                selectedNode.insertBefore(newEl, selectedNode.getFirstChild());
                            } catch (Exception e) {
                                selectedNode.appendChild(newEl);
                            }break;
                        }else{
                            break;
                        }
                    }else{
                        root.appendChild(newEl);
                        break;
                    }
                default:
                    if ((n.getTagName().equals("block")||(n.getTagName().equals("header")))){
                        changeCount++;
                        addParameter("Param");
                        changeCount--;
                        n = (Element) getLastFromDoc(root);
                    }
                    if (!checkHierarchyPriority(n)){
                        insertNodeAfterChildNode(n,newEl);
                        break;
                        //newEl.insertBefore(n, newEl);
                        //n = (Element) n.getParentNode();
                    }
                    try{
                        n.insertBefore(newEl, n.getFirstChild());
                    }catch (Exception e){
                        n.appendChild(newEl);
                    }
                    //insertNodeAfterChildNode((Element) n.getParentNode(),newEl);


            }
        }catch (ClassCastException | IOException e) {
            Element n = (Element) getLastFromDoc(root);
            n.appendChild(newEl);
        }
        selectedNode = newEl;
        try{
            selectedNode.getParentNode().insertBefore(ProgramDocument.createTextNode("\n"),selectedNode);
        }catch (Exception e){

        }
        if (newEl.getNodeName().equals("voltMeas")||newEl.getNodeName().equals("currMeas")
                ||newEl.getNodeName().equals("param")) {
            reWriteResultsIfHaveIt(getParentElementByName(selectedNode, "block").getChildNodes());
        }

        //changeCount%=maxChangesCountBeforeTempSaving;
        if (needTempFileMaking){
            reloadProgramFromDoc();
            changeCount++;
        }
    }
    private void addElement2Doc(Element newEl){
        addElement2Doc(newEl, true);
    }

    private Element getParentElementByName(Node childNode, String blockName) {
        Node parent = childNode.getParentNode();
        while(!parent.getNodeName().equals(blockName)&& !parent.getNodeName().equals("prg")){
            parent=parent.getParentNode();
        }
        return (Element) parent;
    }

    void refreshGUIObjectsByNode(Node node, boolean en){
        try {
            switch (node.getNodeName()) {
                case "switch":
                    Relay r = (Relay) this.GUIObjects.get("relays").get(node.getAttributes().getNamedItem("id").getNodeValue());
                    //Log.info((r.getRelay_status()?"1":"0")+":"+node.getAttributes().getNamedItem("status").getNodeValue());
                    if ((r.getRelay_status()?"1":"0").equals(node.getAttributes().getNamedItem("status").getNodeValue())){
                        r.show();
                    }else r.change_state();
                    break;
                case "voltageSource":
                    PowerSource source = (PowerSource) GUIObjects.get("powerSources").get(node.getAttributes().getNamedItem("id").getNodeValue());
                    for(Map.Entry entry:  GUIObjects.get("powerSources").entrySet()) {
                        PowerSource S = (PowerSource) entry.getValue();
                        S.visibleSelectionIcon(false, "voltageSource");
                    }
                    break;
                case "fullReset":
                    for(Map.Entry entry:  GUIObjects.get("relays").entrySet()) {
                        Relay S = (Relay) entry.getValue();
                        S.toInit();
                    }
                    break;
                default:
            }

            if (node.hasChildNodes()&&en){
                NodeList nList = node.getChildNodes();

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node tNode = nList.item(temp);
                    if (tNode.getNodeType() == Node.ELEMENT_NODE) {
                        refreshGUIObjectsByNode(tNode, true);
                    }
                }
            }
        }catch (Exception e){
            Log.info("ProgramObject: refreshGUIObjectsByNode Error: "+e.fillInStackTrace());
        }
    }

    void initGUIObjects(){
        ArrayList<String> objectsNames = new ArrayList<>();
        objectsNames.add("relays");
        objectsNames.add("powerSources");
        try {
            for (int i=0;i<objectsNames.size();i++){
                Set entrySet = this.GUIObjects.get(objectsNames.get(i)).entrySet();
                Iterator it = entrySet.iterator();

                while(it.hasNext()){
                    Map.Entry me = (Map.Entry)it.next();
                    switch (objectsNames.get(i)){
                        case "relays":
                            Relay r = (Relay) me.getValue();
                            r.toInit();
                            break;
                        case "powerSources":
                            PowerSource S = (PowerSource) me.getValue();
                            S.toInit();
                            break;
                        default:
                    }
                }
            }
        }catch (Exception e){
            Log.info("ProgramObject: initGUIObjects Error: "+e.fillInStackTrace());
        }
    }
    private void makeStream() {
        //формирует исполняемую часть программы из разовых действий
        try {
            clearOldStreams();
            streamBlock = ProgramDocument.createElement("stream");
            headers = ProgramDocument.getElementsByTagName("header");
            boolean enableAdding = !(ExecutingBounds.hasStartNode);
            streamMakeTerminate = false;
            Add2streamBlockGenerationFromNodeList(ProgramDocument.getDocumentElement(), "root", enableAdding);
            ProgramDocument.getDocumentElement().appendChild(streamBlock);
        }catch (Exception e) {
            Log.info(e.getLocalizedMessage());
        }
    }

    private void clearOldStreams() {
        NodeList streams = ProgramDocument.getElementsByTagName("stream");
        for (int i = 0; i < streams.getLength(); i++){
            ProgramDocument.getDocumentElement().removeChild(streams.item(i));
        }

    }

    /*private static final Set<String> oneActionTegs = new HashSet<String>(Arrays.asList(
            new String[] {"switch","pause","fullReset","voltageSource","d_block","voltageSourceReset"}
    ));*/
    private static final Set<String> moreActionTegs = new HashSet<String>(Arrays.asList(
            new String[] {"block","param","header"}
    ));

    private void Add2streamBlockGenerationFromNodeList(Node parentNode, String blockName, boolean enableAdding) {
        if (streamMakeTerminate){
            return;
        }
        try {
            NodeList nList = parentNode.getChildNodes();

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node node = nList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    if (!(moreActionTegs.contains(node.getNodeName()))){
                        if (node.getNodeName().equals("d_block")){
                            String blockID = node.getAttributes().getNamedItem("pro_id").getNodeValue();
                            NodeList blocks = ProgramDocument.getElementsByTagName("block");
                            for(int i=0;i<blocks.getLength();i++){
                                Node tempBlock = blocks.item(i);
                                if (tempBlock.getAttributes().getNamedItem("id").getNodeValue().equals(blockID)){
                                    Add2streamBlockGenerationFromNodeList(tempBlock, node.getAttributes().getNamedItem("name").getNodeValue(), enableAdding);
                                    break;
                                }
                            }
                        }else{
                            if (enableAdding){
                                Node addedNode = setMetaInfoIfNeedAndClone(node, parentNode, blockName, false);
                                if (!streamMakeTerminate) streamBlock.appendChild(addedNode);
                            }
                        }
                    }
                    if (enableAdding){
                        enableAdding = !ExecutingBounds.isEndNode(node);
                        if (!enableAdding){
                            streamMakeTerminate = true;
                        }
                    }else{
                        enableAdding = ExecutingBounds.isStartNode(node);
                    }
                    boolean needKeepChildren = node.hasChildNodes()
                            && !node.getNodeName().equals("result")
                            && !node.getNodeName().equals("Vac");
                    if (needKeepChildren) {
                        if (node.getNodeName().equals("block")){
                            blockName = node.getAttributes().getNamedItem("name").getNodeValue();
                        }
                        Add2streamBlockGenerationFromNodeList(node, blockName, enableAdding);
                    }
                }
            }
        }catch (Exception e){
            Log.info("Add2streamBlockGenerationFromNodeList Error: "+e.fillInStackTrace());
        }

    }

    private Node setMetaInfoIfNeedAndClone(Node sourceNode, Node parentNode, String blockName, boolean cloneDeep){
        String nodeName = sourceNode.getNodeName();
        Element result = (Element) sourceNode.cloneNode(cloneDeep);
        switch (nodeName){
            case "result":
            case "Vac":
                String paramName = parentNode.getAttributes().getNamedItem("name").getNodeValue();
                result.setAttribute("paramName", paramName);
                result.setAttribute("blockName", blockName);
                result.setAttribute("headerText", Methods.getNearHeaderTextForNodeFromHeaderList(sourceNode, headers));
            default:
                return result;
        }
    }

    public void setCurrentInputData(Map<String, String> dataIn) {
        Element root = (Element) ProgramDocument.getElementsByTagName("prg").item(0);
        for (Map.Entry<String, String> set : dataIn.entrySet()) {
            root.setAttribute(set.getKey(),set.getValue());
        }
    }

    public Map<String, String> getProgramDataIn() {
        Element root = (Element) ProgramDocument.getElementsByTagName("prg").item(0);
        try {
            Map<String, String> DataIn = new HashMap<String, String>()
            {
                {
                    put("productName", root.getAttribute("productName"));
                    put("developerName",root.getAttribute("developerName"));
                    put("adapterCode",root.getAttribute("adapterCode"));
                    put("chipSizeX", root.getAttribute("chipSizeX"));
                    put("chipSizeY", root.getAttribute("chipSizeY"));
                };
            };
            return DataIn;
        }catch (Exception e){
            Map<String, String> DataIn = new HashMap<String, String>()
            {
                {
                    put("productName", "");
                    put("developerName", "");
                    put("adapterCode", "");
                    put("chipSizeX", "0");
                    put("chipSizeY", "0");
                };
            };
            return DataIn;
        }
    }
    public void delAllNodes() {
        addTempFile();
        Node root = ProgramDocument.getElementsByTagName("prg").item(0);
        NodeList nodes = root.getChildNodes();
        for (int i=nodes.getLength()-1;i>=0;i--){
            Node tempNode = nodes.item(i);
            if (tempNode.getNodeType()==Node.ELEMENT_NODE){
                root.removeChild(tempNode);
            }
        }
        reloadProgramFromDoc();
        changeCount++;
        blockCount=0;
        ((Pane) GUIObjects.get("panels").get("workPane")).setVisible(false);
    }
    public void delOneNode(Node selectedNode){
        Log.info("Select DelFunc for: "+ selectedNode.getNodeName());
        Node prevNode = getPrevious(selectedNode);

        if (selectedNode.getNodeName().equals("block"))
            blockCount--;
        if (blockCount<=0){
            blockCount = 0;
            ((Pane) GUIObjects.get("panels").get("workPane")).setVisible(false);
        }
        String deletingNodeName = selectedNode.getNodeName();
        Node parentBlock = getParentElementByName(selectedNode, "block");
        selectedNode.getParentNode().removeChild(selectedNode);
        this.selectedNode = prevNode;
        if (deletingNodeName.equals("voltMeas")||deletingNodeName.equals("currMeas")
                ||deletingNodeName.equals("param")) {
            reWriteResultsIfHaveIt(parentBlock.getChildNodes());
        }
    }
    public void delSelectedNodes(){
        addTempFile();
        ProgramSaved = false;
        try {
            List<TreeItem<String>> selectedItems = struct_tree.getSelectionModel().getSelectedItems();
            List<Node> needDeleteNodes = new ArrayList<>();
            for (TreeItem<String> item : selectedItems){
                try {
                    if (item == null) continue;
                    ArrayList<Integer> indexMap = Methods.getIndexMapForSelectedTreeItem(item);
                    Node node = Methods.getNodeFromDocumentByIndexMap(ProgramDocument, indexMap, selfLink);
                    needDeleteNodes.add(node);
                }catch (Exception ex){
                    ex.printStackTrace();
                    continue;
                }
            }
            for (Node node: needDeleteNodes){
                delOneNode(node);
            }
        }catch (Exception ex){
            Log.info("ProgramObjectClass::delSelectedNodes error: " + ex.getMessage());
        }
        changeCount++;
        reloadProgramFromDoc();
    }
    private void addTempFile() {
        String nameTempFile = "tempPrg";//+String.valueOf(changeCount+1);
        try {
            File tempFile = File.createTempFile(nameTempFile,".tempAIK");
            tempFile.deleteOnExit();
            workWithXMLStructure.saveXMLDocument2FileByFilePath(ProgramDocument,tempFile.getAbsolutePath());
            int index = changeCount% MAX_CHANGES_COUNT_BEFORE_TEMP_SAVING;
            if (tempFiles[index]!=null)
                tempFiles[index].delete();
            tempFiles[index] = tempFile;
            if (allowUndoCounter< MAX_CHANGES_COUNT_BEFORE_TEMP_SAVING -1){
                allowUndoCounter++;
            }
        } catch (IOException | TransformerException e) {
            Log.info("ProgramObjectClass addTempFile Error"+e.fillInStackTrace());
        }
    }

    private File getLastTempFile() {
        int index = changeCount% MAX_CHANGES_COUNT_BEFORE_TEMP_SAVING;
        return tempFiles[index>0?index-1: MAX_CHANGES_COUNT_BEFORE_TEMP_SAVING -1];
    }

    public void getLibrary() {

        String path = PathHolder.getLibPath();
        if(path == null || path.equals("no path")){
            path = selectLibPath();
        }

        if (!path.equals("no path")){
            ArrayList<String> libFileList = Methods.listFilesForFolder(new File(path), ".xml");
            try{
                Stage dialog = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("views/libraryWork.fxml"));
                Parent root = loader.load();
                dialog.setScene(new Scene(root));
                dialog.initModality(Modality.APPLICATION_MODAL);
                LibController controller = loader.<LibController>getController();

                //чтобы исключить пропуск измерений
                if (selectedNode!=null) {
                    if (selectedNode.getNodeName().equals("param"))
                        selectedNode = selectedNode.getLastChild();
                    if (selectedNode.getParentNode().getNodeName().equals("param"))
                        selectedNode = selectedNode.getParentNode().getLastChild();
                }
                controller.setStartData(path, libFileList, getMeasCount());
                dialog.showAndWait();
                if(controller.getDialog_status().equals("yes")) {
                    Element newEl = controller.getResultEl();
                    newEl.setAttribute("id", Integer.toString(blockCount++));
                    this.ProgramDocument.adoptNode(newEl);
                    this.addElement2Doc(newEl);
                    ((Pane) GUIObjects.get("panels").get("workPane")).setVisible(true);
                }
            }catch (IOException e){
                Log.info("GUI Error dataInputButt"+e.getLocalizedMessage());
            }
        }
        else{
            try {
                if (Methods.twoStateDialog("Повторить операцию?") == DialogEndStatus.YES){
                    getLibrary();
                }
            } catch (IOException e) {
                Log.info("ProgramObjectClass getLibrary Error"+e.fillInStackTrace());
            }
        }
    }

    public String selectLibPath() {
        String path = Methods.getPath2library(PathHolder.getLibPath());
        PathHolder.setLibPath(path);
        return path;
    }

    public void resetExecutingBounds() {
        ExecutingBounds.setHasEndNode(false);
        ExecutingBounds.setHasStartNode(false);
        reloadProgramFromDoc();
    }
}
