package sample.controllers;

import java.net.URL;
import javafx.fxml.FXML;

import java.util.ResourceBundle;

import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class doubleBlockController {
    private NodeList Blocks;
    private String dialog_status = "open";
    public String getDialog_status() {
        return dialog_status;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField textField;

    @FXML
    private ListView<String> blocksList;

    @FXML
    void initialize() {
        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });

    }
    public void sendBlockNodes(NodeList blocks){
        this.Blocks = blocks;
        blocksList.getItems().clear();
        for(int i = 0; i<Blocks.getLength();i++){
            blocksList.getItems().add(Blocks.item(i).getAttributes().getNamedItem("name").getNodeValue());
        }
        blocksList.getSelectionModel().select(0);
    }
    private void yesEnd() {
        if (check()) {
            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }
    }

    private boolean check() {
        return !getBlockName().isEmpty();
    }

    public String getBlockName(){
        return textField.getText();
    }
    public String getPrototypeBlockId(){
        Node selNode = Blocks.item(blocksList.getSelectionModel().getSelectedIndex());
        return selNode.getAttributes().getNamedItem("id").getNodeValue()+";"+selNode.getAttributes().getNamedItem("name").getNodeValue();
    }
}
