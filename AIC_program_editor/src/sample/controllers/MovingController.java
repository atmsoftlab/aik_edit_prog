package sample.controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.w3c.dom.Element;

public class MovingController {
    private String dialog_status = "open";
    private Map<String, String> movingVariants = new HashMap<String, String>()
    {
        {
            put("Move", "Относительно");
            put("Set", "Абсолютно");
            put("SetBP", "Задать БТ");
        };
    };
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private ChoiceBox<String> movingMethodsCB;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField moveX;

    @FXML
    private CheckBox setStepCheckBox;

    @FXML
    private TextField moveY;

    @FXML
    void initialize() {
        dialog_yes_butt.setText("Да");
        dialog_no_butt.setText("Нет");
        dialog_Label.setWrapText(true);
        fillMovingMethodsChoiceBox();


        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });
        moveX.setOnKeyPressed(ke->{
            KeyCode keyCode = ke.getCode();
            if (keyCode.equals(KeyCode.ENTER)){
                yesEnd();
            }
        });
        moveY.setOnKeyPressed(ke->{
            KeyCode keyCode = ke.getCode();
            if (keyCode.equals(KeyCode.ENTER)){
                yesEnd();
            }
        });
    }

    public String getMoving() {
        return getTeg4SelectedMovingMethodsItem()+";"+moveX.getText()+";"+moveY.getText()+";"+(setStepCheckBox.isSelected()==false?"um":"k");
    }

    private void yesEnd(){
        if (inputCheck()) {
            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }
    }

    boolean inputCheck(){
        boolean result = true;
        try {
            Integer.parseInt(moveX.getText());
        }catch (Exception e){
            moveX.setText("Ошибка!");
            result&=false;
        }
        try {
            Integer.parseInt(moveY.getText());
        }catch (Exception e){
            moveY.setText("Ошибка!");
            result&=false;
        }
        return result;
    }

    public String getDialog_status() {
        return dialog_status;
    }

    public void setMoving(String movingStr) {
        String[] params = movingStr.split(";");
        movingMethodsCB.setValue(movingVariants.get(params[0]));
        moveX.setText(params[1]);
        moveY.setText(params[2]);
        setStepCheckBox.setSelected(params[3].equals("k"));
    }
     private void fillMovingMethodsChoiceBox(){
        /**
         *Заполняет варианты метода перемещения
         **/
        movingMethodsCB.getItems().clear();
        for (Object item:movingVariants.keySet().toArray()){
            movingMethodsCB.getItems().add(movingVariants.get((String) item));
        }
        movingMethodsCB.setValue(movingVariants.get("Move"));
     }
     private String getTeg4SelectedMovingMethodsItem(){
        /**
         * Возвращает тег для нижнего уровня соответсвующий
         * выбранному типу перемещения
         **/
        String result = "Move";
         for (Object item:movingVariants.keySet().toArray()){
             if (movingVariants.get(item.toString()).equals(movingMethodsCB.getValue())){
                 result = item.toString();
             }
         }
        return result;
     }
}
