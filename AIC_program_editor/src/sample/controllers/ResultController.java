package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

import com.sun.javafx.scene.control.skin.TextAreaSkin;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sample.Methods;
import sample.ResultReIndexMaker;
import sample.enums.DialogEndStatus;

public class ResultController {
    private static final String DEFAULT_DIGIT_COUNT = "3";
    private final String[] SPACIAL_SYMBOLS = new String[]{"\\[", "\\]", "\\{", "\\}", "\\(", "\\)"};

    private HashMap<String,Object> values = new HashMap<String,Object>();
    private List<Node> measNodes;

    public HashMap<String, String> getOutValues() {
        return OutValues;
    }

    private HashMap<String,String> OutValues = new HashMap<String,String>();
    private DialogEndStatus dialog_status = DialogEndStatus.OPEN;
    public DialogEndStatus getDialog_status() {
        return dialog_status;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField minNorm;

    @FXML
    private CheckBox NormsCheckBox;

    @FXML
    private TextField maxNorm;

    @FXML
    private ListView<String> valuesList;

    @FXML
    private TextArea expressionIn;

    @FXML
    private TextField unit;

    @FXML
    private TextField afterPointDigitCount;

    @FXML
    void initialize() {
        NormsCheckBox.setSelected(false);
        afterPointDigitCount.setText(DEFAULT_DIGIT_COUNT);
        values.put("indexArrayList", new ArrayList<Integer>());
        values.put("valuesArrayList", new ArrayList<String>());
        valuesList.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent click) {

                if (click.getClickCount() == 2) {
                    String value = valuesList.getSelectionModel().getSelectedItem();
                    ((ArrayList<Integer>)values.get("indexArrayList")).add(valuesList.getSelectionModel().getSelectedIndex());
                    ((ArrayList<String>)values.get("valuesArrayList")).add(value);
                    expressionIn.insertText(expressionIn.getCaretPosition(), value);

                }
            }
        });
        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = DialogEndStatus.NO;
            stage.close();
        });

    }

    public void yesEnd() {
        if (checkForm()){
            OutValues.put("IndExp",GetIndexedExpString(expressionIn.getText()));
            OutValues.put("Exp",expressionIn.getText());
            OutValues.put("measCount", Integer.toString(valuesList.getItems().size()));
            OutValues.put("minNorm",minNorm.getText());
            OutValues.put("maxNorm",maxNorm.getText());
            OutValues.put("normed",NormsCheckBox.isSelected()?"+":"-");
            OutValues.put("unit", unit.getText());
            OutValues.put("afterDigitCount", afterPointDigitCount.getText());

            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = DialogEndStatus.YES;
            stage.close();
        }
    }

    private boolean checkForm() {
        Boolean result = true;
        //проверка выражения
        result&=checkExp(expressionIn.getText());
        //проверка норм
        if (result && NormsCheckBox.isSelected()){
            result&= Methods.checkFloat(maxNorm);
            result&=Methods.checkFloat(minNorm);
            if (maxNorm.getText().equals(minNorm.getText())){
                result&=false;
                try {
                    Methods.messageDialog("Нормы не должны быть равны!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        result&= Methods.checkInt(afterPointDigitCount);
        return result;
    }

    private Boolean checkExp(String text) {
        String e = "([Ee][+-]\\d+)*";
        String operators = "\\+-/\\*";
        String someMeas =  getMeasStringsRegExpString();
        String expRegex = "(\\(*\\s*(\\d*"+ e +"\\s*["+ operators +"]\\s*)*"+ someMeas+"?\\s*\\)*)";
        String expRegex1 = "(\\(*\\s*"+ someMeas+"?\\s*(["+ operators +"]\\s*\\d*"+ e +"\\s*)*\\)*)";
        String expRegex2 = "(\\(*\\s*(["+ operators +"]*\\s*\\d*"+ e +"\\s*)*\\)*)";
        text = text.trim();
        boolean result = Pattern.matches("("+expRegex+"|"+ expRegex1 +"|"+ expRegex2 + ")+", text);
        if (result){
            try {
                result = !Pattern.matches(".*#\\d#\\d.*", GetIndexedExpString(text));
            }catch (IllegalArgumentException indexedError){
                result = false;
            }
        }
        if (!result){
            try {
                Methods.messageDialog("Ошибка в выражении!");
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        }
        return result;
    }

    private String getMeasStringsRegExpString() {
        String joined =  valuesList.getItems().stream()
                .reduce((s1, s2) -> String.join("|", s1, s2)).get();

        for (String spatial : SPACIAL_SYMBOLS){
            joined = joined.replaceAll(spatial, "\\\\"+spatial);
        }
        return "(" + joined + ")";
    }

    private String GetIndexedExpString(String text){
        return ResultReIndexMaker.GetIndexedExpString(text, measNodes);
    }
    public void setValues(Map<String, String> values) {
        expressionIn.setText(values.get("Exp"));
        boolean isNormed = values.getOrDefault("normed", "-").equals("+");
        NormsCheckBox.setSelected(isNormed);
        minNorm.setText(values.get("minNorm"));
        maxNorm.setText(values.get("maxNorm"));
        unit.setText(values.get("unit"));
        afterPointDigitCount.setText(values.getOrDefault("afterDigitCount", DEFAULT_DIGIT_COUNT));
    }
    public void sendMeasListData(List<Node> measNodes) {
        this.measNodes = measNodes;
        valuesList.getItems().clear();
        for (String measStr : ResultReIndexMaker.getMeasStringList(measNodes)){
           valuesList.getItems().add(measStr);
        }
    }

    private String sameItemsCountAdder(String[] strArr2){
        ListIterator<String> iterator = valuesList.getItems().listIterator();
        int counter = 1;
        while (iterator.hasNext()) {
            String item = iterator.next();
            if (item.contains(strArr2[0])&&item.contains(strArr2[1])){
                counter++;
            }
        }
        return strArr2[0]+(counter>1?"_"+Integer.toString(counter):"")+strArr2[1];
    }
}
