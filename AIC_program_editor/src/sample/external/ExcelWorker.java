package sample.external;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.collections.ObservableList;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import sample.Methods;
import sample.Result;
import sample.simple.ResultParamKey;

/**
 * A simple Java program that exports data from database to Excel file.
 * @author Nam Ha Minh
 * (C) Copyright codejava.net
 */
public class ExcelWorker {
    private static final int MEASURE_INDEX_IN_HEADERS = 5;
    private static final int HEADER_START_INDEX = 1;
    public static final boolean USE_POINT = true;
    private static List<ResultParamKey> mapKeys;
    private static Map<ResultParamKey, List<Result>> resultMap;
    private static List<String> originBlocks;
    private static int measColumnCount = 0;

    public static boolean exportFromTableViewItems(String excelFilePath,
                                                   Map<ResultParamKey, List<Result>> resultParamKeyListMap,
                                                   List<ResultParamKey> sortedKeys,
                                                   List<String> originBlockNames,
                                                   String[] headers, int columnCountForEqualParamResults) throws IOException {
        resultMap = resultParamKeyListMap;
        mapKeys = sortedKeys;
        originBlocks = originBlockNames;
        measColumnCount = columnCountForEqualParamResults >= originBlockNames.size()
                ? columnCountForEqualParamResults : originBlockNames.size();

        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Результаты");

            writeHeaderLine(sheet, headers);

            writeDataLines(workbook, sheet);

            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
            return true;
        } catch (IOException e) {
            System.out.println("File IO error:" + e.getMessage());
            Methods.messageDialog("Не удалось сохнить файл отчета! Убедитесь, что файл отчета не открыт в другой программе!");
            return false;
        }
    }

    private static void writeHeaderLine(XSSFSheet sheet, String[] headers) {
        Row headerRow = sheet.createRow(0);
        int offset = 0;
        for (int i = HEADER_START_INDEX; i < headers.length; i++){
            int headerCellIndex = i + offset;
            if (i == MEASURE_INDEX_IN_HEADERS){
                for(int j = 0; j < measColumnCount; j++){
                    Cell headerCell = headerRow.createCell(headerCellIndex + j - HEADER_START_INDEX);
                    headerCell.setCellValue(j<originBlocks.size()?originBlocks.get(j):headers[i] + String.valueOf(j+1));
                }
                offset += measColumnCount - 1;
                continue;
            }
            Cell headerCell = headerRow.createCell(headerCellIndex - HEADER_START_INDEX);
            headerCell.setCellValue(headers[i]);
        }
    }

    private static void writeDataLines(XSSFWorkbook workbook,
                                XSSFSheet sheet) {
        int rowCount = 1;

        for (ResultParamKey resultParamKey : mapKeys){
            Row row = sheet.createRow(rowCount++);
            List<Result> results = resultMap.get(resultParamKey);

            int columnCount = 0;
            Cell cell = row.createCell(columnCount++);
            cell.setCellValue(resultParamKey.getHeaderText());

            cell = row.createCell(columnCount++);
            cell.setCellValue(resultParamKey.getParamName());

            CellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setDataFormat((short) 1);

            cell = row.createCell(columnCount++);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(formatDecimalString(resultParamKey.getMinNorm()));

            cell = row.createCell(columnCount++);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(formatDecimalString(resultParamKey.getMaxNorm()));

            CellStyle valueCellStyle = workbook.createCellStyle();
            valueCellStyle.setDataFormat((short) 1);
            for(int i = 0; i < measColumnCount; i++){
                cell = row.createCell(columnCount+i);
                cell.setCellValue("-");
            }
            for (Result result : results) {
                int blockNameIndex = originBlocks.indexOf(result.getBlockName());
                if (blockNameIndex >= 0){
                    cell = row.createCell(columnCount+blockNameIndex);
                    valueCellStyle.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
                    boolean isInNorm = result.isInNorm();
                    if (!isInNorm){
                        valueCellStyle.setFillForegroundColor(IndexedColors.CORAL.getIndex());
                        valueCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    }
                    cell.setCellStyle(valueCellStyle);
                    cell.setCellValue(formatDecimalString(result.getMeasure()));
                }
            }
            columnCount += measColumnCount;

            cell = row.createCell(columnCount++);
            cell.setCellValue(resultParamKey.getUnit());

            cell = row.createCell(columnCount++);
            cell.setCellValue(results.get(0).getDateTime());
        }
    }

    private static String formatDecimalString(String decimal){
        if (USE_POINT){
            return decimal.replace(',', '.');
        }
        return decimal;
    }
}
