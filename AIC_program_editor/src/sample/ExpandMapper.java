package sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExpandMapper {

    public static final boolean DEFAULT_EXPAND = false;
    private static Map<ArrayList<Integer>, Boolean> expandMap = new HashMap<>();

    public static void changeOrSetMapItem(ArrayList<Integer> indexMap, boolean expanded){
        expandMap.put(indexMap, expanded);
    }

    public static boolean getExpanded(ArrayList<Integer> indexMap){
        return expandMap.getOrDefault(indexMap, DEFAULT_EXPAND);
    }

}
