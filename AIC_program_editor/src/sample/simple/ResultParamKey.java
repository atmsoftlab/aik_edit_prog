package sample.simple;

import sample.Result;

import java.util.Objects;

public class ResultParamKey {
    private String blockName;
    private String paramName;
    private String headerText;
    private String minNorm;
    private String maxNorm;
    private String unit;
    private int number;

    public ResultParamKey(Result result){
        blockName = result.getBlockName();
        paramName = result.getParamName();
        headerText = result.getHeaderText();
        minNorm = result.getMinNorm();
        maxNorm = result.getMaxNorm();
        unit = result.getUnit();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResultParamKey that = (ResultParamKey) o;
        return number == that.number && Objects.equals(paramName, that.paramName) && Objects.equals(headerText, that.headerText) && Objects.equals(minNorm, that.minNorm) && Objects.equals(maxNorm, that.maxNorm) && Objects.equals(unit, that.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paramName, headerText, minNorm, maxNorm, unit, number);
    }

    public String getBlockName() {
        return blockName;
    }

    public String getParamName() {
        return paramName;
    }

    public String getHeaderText() {
        return headerText;
    }

    public String getMinNorm() {
        return minNorm;
    }

    public String getMaxNorm() {
        return maxNorm;
    }

    public String getUnit() {
        return unit;
    }
}
