package sample;


import javafx.scene.Scene;
import javafx.scene.shape.Circle;

import java.awt.*;

public class Relay {

    private String relay_name;
    private String relay_id;
    public boolean relay_status;
    private boolean init_relay_status;
    private Circle[] nodes;


    Relay(String name, String id)
    {
        this.relay_name = name;
        this.relay_id = id;
        //this.node = n;
        this.relay_status = false;
    }

    Relay(String name, String id, boolean Status)
    {
        this.relay_name = name;
        this.relay_id = id;
        //this.node = n;
        this.relay_status = Status;
    }


    public void setNodes(Circle[] nodes) {
        this.nodes = nodes;
    }

    public boolean getRelay_status() {
        return relay_status;
    }

    public Circle[] getNodes() {
        return this.nodes;
    }

    public String getRelay_id(){
        return relay_id;
    }

    void change_state(){
        this.relay_status = !this.relay_status;
        this.show();
    }
    void show(){
        for (Circle node:nodes) {
            node.setStyle(this.relay_status ? "-fx-fill: black;" : "-fx-fill: white;");
        }
    }
    public void visibleSelectionIcon(boolean b){
        for (Circle node:nodes) {
            node.setStyle(b&this.relay_status?"-fx-fill: red;":(this.relay_status ? "-fx-fill: black;" : "-fx-fill: white;"));
        }
    }
    void toInit(){
        this.relay_status=this.init_relay_status;
        show();
    }
    String displayInfo(){
        return relay_name;
    }
}

