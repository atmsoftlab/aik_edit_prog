package sample.enums;

public enum DialogEndStatus {
    OPEN, YES, NO, CLOSE
}
