package sample;

public class Result {
    private static int counter = 0;

    private String number;
    private String blockName;
    private String paramName;
    private String headerText;
    private String minNorm;
    private String maxNorm;
    private String unit;
    private String measure;
    private String dateTime;
    private boolean isLastSuccess = false;
    public Result(String paramName, String headerText, String minNorm, String maxNorm, String unit, String measure, String dateTime){
        this.number = String.valueOf(++counter);
        this.paramName = paramName;
        this.headerText = headerText;
        this.minNorm = minNorm;
        this.maxNorm = maxNorm;
        this.unit = unit;
        this.measure = measure;
        this.dateTime = dateTime;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public static void setCounter(int counter) {
        Result.counter = counter;
    }

    public String getUnit() {
        return unit;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getMeasure() {
        return measure;
    }

    public static int getCounter() {
        return counter;
    }

    public String getNumber() {
        return number;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getParamName() {
        return paramName;
    }

    public String getMinNorm() {
        return minNorm;
    }

    public String getMaxNorm() {
        return maxNorm;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getHeaderText() {
        return headerText;
    }

    public boolean isLastSuccess() {
        return isLastSuccess;
    }

    public void setLastSuccess(boolean lastSuccess) {
        isLastSuccess = lastSuccess;
    }

    public boolean isInNorm(){
        double value;
        double minNorm;
        double maxNorm;
        try {
            minNorm = Double.parseDouble(this.minNorm);
            maxNorm = Double.parseDouble(this.maxNorm);
            value = Double.parseDouble(this.measure.replace(',', '.'));
        }catch (Exception parseException){
            return true;
        }
        return value >= minNorm && value <= maxNorm;
    }
}
