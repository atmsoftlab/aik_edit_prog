def initdb(path):
    import os
    import xml.etree.ElementTree as etr

    keys = ''
    initdonn = {}

    if os.path.exists(path):
        tree = etr.parse(path)
        root = tree.getroot()
        stat = 0

        # for child_of_root in root.iter():
        #     print('Tag: %snKeys: %snItems: %snText: %sn' % (child_of_root.tag, child_of_root.keys(), child_of_root.items(), child_of_root.text))

        for child_of_root in root.iter():
            keys = child_of_root.items()
            if len(keys) != 0:
                break

        if keys != '':
            initdonn = {'Database': keys[1][1].split(';')[0].partition('=')[2],
                        'Server': keys[1][1].split(';')[1].partition('=')[2],
                        'Port': keys[1][1].split(';')[2].partition('=')[2],
                        'UserId': keys[1][1].split(';')[3].partition('=')[2],
                        'Password': keys[1][1].split(';')[4].partition('=')[2]}
        else:
            stat = 2

        for i in range(0, len(list(initdonn.values()))):
            if list(initdonn.values())[i] == '':
                stat = 2

    else:
        stat = 1

    return initdonn, stat



