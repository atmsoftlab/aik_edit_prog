def classification(resarray):
    test_plan_status = 0
    massive_sort_class = []

    for i in range(0, len(resarray)):
        if resarray[i][2].get('Status') == 'ГОДЕН':
            test_plan_status = 0
        else:
            test_plan_status = 1
            break

    if test_plan_status == 0:
        for i in range(0, len(resarray)):
            massive_sort_class.append(resarray[i][3].get('FinalGroup'))

        if len(massive_sort_class) == 0:
            result_group = ''
        else:
            result_group = max(massive_sort_class)
    else:
        result_group = ''

    return test_plan_status, result_group
